/*
export const someMutation = (state) => {}
 */

export const message = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'UPDATE') {
    let { channel } = pkg
    if (channel === 'lobby') {
      state.lobby.push(pkg)
    }
    if (channel === 'game') {
      state.game.push(pkg)
    }
  }
}

export const reset = (state, payload) => {
  let { type } = payload
  if (type === 'GAME') {
    state.game = []
  }
}
