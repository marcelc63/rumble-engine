/*
export const someAction = (state) => {}
 */

export const message = ({ state, commit }, payload) => {
  let { message } = payload
  commit('message', { type: 'UPDATE', pkg: { ...message } })
}

export const reset = ({ state, commit }, payload) => {
  commit('reset', { type: 'GAME' })
}
