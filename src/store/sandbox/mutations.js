import _ from 'lodash'

/*
export const someMutation = (state) => {
}
*/

export const char = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'CHAR_CREATE') {
    state.char = pkg
    console.log(state.char)
  }
  if (type === 'CHAR_SAVE') {
    state.char = {
      ...pkg,
      skills: state.char.skills
    }
  }
  if (type === 'CHAR_UPDATE') {
    state.char = pkg
  }
}

export const skill = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'SKILL_ADD') {
    let char = state.char
    char.skills = char.skills.concat(pkg)
    state.char = char
  }
  if (type === 'SKILL_REMOVE') {
    let char = state.char
    char.skills = char.skills.filter((x, i) => x.id !== pkg.id)
    state.char = char
  }
  if (type === 'SKILL_UPDATE') {
    let char = state.char
    let page = state.page
    let skill = char.skills[page.skill][pkg.subject]
    if (Array.isArray(skill)) {
      char.skills[page.skill][pkg.subject][0].value = pkg.value
    } else {
      char.skills[page.skill][pkg.subject] = pkg.value
    }
  }
  if (type === 'SKILL_COST') {
    let char = state.char
    let page = state.page
    char.skills[page.skill].cost[pkg.subject][0].value = pkg.value
  }
}

export const effect = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'EFFECT_ADD') {
    let char = state.char
    let skill = char.skills[pkg.skill]
    skill.effects = skill.effects.concat(pkg.effect)
    state.char = char
  } else if (type === 'EFFECT_REMOVE') {
    let char = state.char
    let skill = char.skills[pkg.skill]
    skill.effects = skill.effects.filter(x => x.id !== pkg.id)
    state.char = char
  } else if (type === 'EFFECT_UP') {
    let char = state.char
    let skill = char.skills[pkg.skill]
    let effects = skill.effects
    let from = effects.findIndex(x => x.id === pkg.id)
    if (from > 0) {
      let to = from - 1
      effects.splice(to, 0, effects.splice(from, 1)[0])
    }
  } else if (type === 'EFFECT_DOWN') {
    let char = state.char
    let skill = char.skills[pkg.skill]
    let effects = skill.effects
    let from = effects.findIndex(x => x.id === pkg.id)
    if (from < effects.length - 1) {
      let to = from + 1
      effects.splice(to, 0, effects.splice(from, 1)[0])
    }
  } else if (type === 'EFFECT_UPDATE') {
    let char = state.char
    let page = state.page
    let effect = char.skills[page.skill].effects[page.effect][pkg.subject]
    if (Array.isArray(effect)) {
      char.skills[page.skill].effects[page.effect][pkg.subject][0].value =
        pkg.value
    } else {
      char.skills[page.skill].effects[page.effect][pkg.subject] = pkg.value
    }
  }
}

export const after = (state, payload) => {
  let { type, pkg, skill, effect } = payload
  if (type === 'AFTER_ADD') {
    let char = state.char
    let skills = char.skills[skill]
    let effects = char.skills[skill].effects[effect]
    effects.after = effects.after.concat(pkg)
    state.char = char
  }
}

export const evaluate = (state, payload) => {
  let { type, pkg, skill, effect } = payload
  if (type === 'EVALUATE_DELETE') {
    let char = state.char
    let skills = char.skills[skill]
    let model = pkg.model
    let child = pkg.child
    // let effects = char.skills[skill].effects[effect]
    // effects.after = effects.after.concat(pkg)
    if (child === undefined) {
      skills[model] = skills[model].filter((x, i) => i !== pkg.index)
    } else {
      skills[model][child] = skills[model][child].filter(
        (x, i) => i !== pkg.index
      )
    }
    state.char = char
  } else if (type === 'EVALUATE_EFFECT_DELETE') {
    let char = state.char
    let effects = char.skills[skill].effects[effect]
    let model = pkg.model
    // let effects = char.skills[skill].effects[effect]
    // effects.after = effects.after.concat(pkg)
    effects[model] = effects[model].filter((x, i) => i !== pkg.index)
    state.char = char
  }
}

export const view = (state, payload) => {
  let { type, val } = payload
  if (type === 'VIEW_ADD') {
    let view = state.char.view
    if (!view.includes(val) && view.length < 4) {
      view = view.concat(val)
      state.char.view = view
    }
  }
  if (type === 'VIEW_REMOVE') {
    let view = state.char.view
    view = view.filter(x => x !== val)
    state.char.view = view
  }
  if (type === 'VIEW_UP') {
    let view = state.char.view
    let from = view.findIndex(x => x === val)
    if (from > 0) {
      let to = from - 1
      view.splice(to, 0, view.splice(from, 1)[0])
    }
  }
  if (type === 'VIEW_DOWN') {
    let view = state.char.view
    let from = view.findIndex(x => x === val)
    if (from < 4) {
      let to = from + 1
      view.splice(to, 0, view.splice(from, 1)[0])
    }
  }
}

export const charge = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'CHARGE_ADD') {
    state.char.status.onCharge = state.char.status.onCharge.concat(pkg)
  }
  if (type === 'CHARGE_REMOVE') {
    state.char.status.onCharge = state.char.status.onCharge.filter(
      x => x.id !== pkg.id
    )
  }
}

export const arenas = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'LOAD') {
    state.arenas = pkg.arenas
  }
}

export const arena = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'SELECT') {
    state.arena.name = pkg.arena.name
    state.arena.description = pkg.arena.description
    state.arena._id = pkg.arena._id
  }
}

export const chars = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'LOAD') {
    state.chars = pkg.chars
  }
}

export const page = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'SKILL') {
    state.page.skill = pkg.skill
    state.page.context = 'skill'
  }
  if (type === 'EFFECT') {
    state.page.effect = pkg.effect
    state.page.context = 'effect'
  }
}

export const settings = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'JSON') {
    state.settings.json = !state.settings.json
  }
}

export const conditions = (state, payload) => {
  let { type, pkg } = payload
  let model = {
    condition: 'active',
    subject: '',
    direction: '',
    evaluator: '',
    comparison: '',
    value: true
  }
  if (type === 'CONDITIONS_OPEN') {
    state.conditions.modal = true
    state.conditions.subject = pkg.subject
    state.conditions.group = pkg.group
    state.conditions.edit = false
    state.conditions.model = { ...model }
  }
  if (type === 'CONDITIONS_CLOSE') {
    state.conditions.modal = false
    state.conditions.subject = ''
    state.conditions.group = ''
    state.conditions.edit = false
    state.conditions.model = {
      ...model
    }
  }
  if (type === 'CONDITIONS_SAVE') {
    //Char Information
    let char = state.char
    let skills = char.skills
    let skillIndex = state.page.skill
    let effectIndex = state.page.effect

    //Conditions Settings
    let subject = state.conditions.subject
    let model = state.conditions.model
    let editIndex = state.conditions.editIndex
    let edit = state.conditions.edit
    let group = state.conditions.group

    // Update Data
    if (!edit) {
      if (group === 'skill') {
        let skill = skills[skillIndex][subject]
        skill.push(_.cloneDeep(model))
      }
      if (group === 'cost') {
        let cost = skills[skillIndex].cost[subject]
        cost.push(_.cloneDeep(model))
      }
      if (group === 'effect') {
        let effect = skills[skillIndex].effects[effectIndex][subject]
        effect.push(_.cloneDeep(model))
      }
    } else {
      if (group === 'skill') {
        skills[skillIndex][subject][editIndex] = model
      }
      if (group === 'cost') {
        skills[skillIndex].cost[subject][editIndex] = model
      }
      if (group === 'effect') {
        skills[skillIndex].effects[effectIndex][subject] = model
      }
    }
  }
  if (type === 'CONDITIONS_REMOVE') {
    // Char Info
    let char = state.char
    let skills = char.skills
    let skillIndex = state.page.skill
    let effectIndex = state.page.effect

    //Conditions Settings
    let subject = pkg.subject
    let index = pkg.editIndex + 1
    let group = pkg.group

    // Update Data
    if (group === 'skill') {
      let skill = skills[skillIndex][subject]
      skills[skillIndex][subject] = skill.filter((x, i) => i !== index)
    }
    if (group === 'cost') {
      let cost = skills[skillIndex].cost[subject]
      skills[skillIndex].cost[subject] = cost.filter((x, i) => i !== index)
    }
    if (group === 'effect') {
      let effect = skills[skillIndex].effects[effectIndex][subject]
      skills[skillIndex].effects[effectIndex][subject] = effect.filter(
        (x, i) => i !== index
      )
    }
  }
  if (type === 'CONDITIONS_EDIT') {
    state.conditions.modal = true
    state.conditions.subject = pkg.subject
    state.conditions.edit = true
    state.conditions.editIndex = pkg.editIndex + 1
    state.conditions.model = pkg.model
  }
  if (type === 'CONDITIONS_MODEL') {
    state.conditions.model[pkg.subject] = pkg.value
  }
  if (type === 'CONDITIONS_UP') {
    // Char Info
    let char = state.char
    let skills = char.skills
    let skillIndex = state.page.skill
    let effectIndex = state.page.effect

    // Conditions Settings
    let subject = pkg.subject
    let from = pkg.editIndex + 1
    let group = pkg.group

    // Update Data
    if (group === 'skill') {
      let skill = skills[skillIndex][subject]
      if (from > 1) {
        let to = from - 1
        skill.splice(to, 0, skill.splice(from, 1)[0])
      }
    }
    if (group === 'cost') {
      let cost = skills[skillIndex].cost[subject]
      if (from > 1) {
        let to = from - 1
        cost.splice(to, 0, cost.splice(from, 1)[0])
      }
    }
    if (group === 'effect') {
      let effect = skills[skillIndex].effects[effectIndex][subject]
      if (from > 1) {
        let to = from - 1
        effect.splice(to, 0, effect.splice(from, 1)[0])
      }
    }
  }
  if (type === 'CONDITIONS_DOWN') {
    // Char Info
    let char = state.char
    let skills = char.skills
    let skillIndex = state.page.skill
    let effectIndex = state.page.effect

    // Conditions Settings
    let subject = pkg.subject
    let from = pkg.editIndex + 1
    let group = pkg.group

    // Update Data
    if (group === 'skill') {
      let skill = skills[skillIndex][subject]
      if (from < skill.length - 1) {
        let to = from + 1
        skill.splice(to, 0, skill.splice(from, 1)[0])
      }
    }
    if (group === 'cost') {
      let cost = skills[skillIndex].cost[subject]
      if (from < cost.length - 1) {
        let to = from + 1
        cost.splice(to, 0, cost.splice(from, 1)[0])
      }
    }
    if (group === 'effect') {
      let effect = skills[skillIndex].effects[effectIndex][subject]
      if (from < effect.length - 1) {
        let to = from + 1
        effect.splice(to, 0, effect.splice(from, 1)[0])
      }
    }
  }
}
