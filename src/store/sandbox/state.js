export default {
  chars: [],
  char: {
    status: {
      onCharge: []
    }
  },
  page: {
    context: '',
    skill: 0,
    effect: 0
  },
  arenas: [],
  arena: {
    name: '',
    description: '',
    _id: ''
  },
  conditions: {
    modal: false,
    group: '', //Skill or Effect or Cost
    subject: '',
    edit: false,
    editIndex: 0,
    model: {
      condition: 'active',
      subject: '',
      direction: '',
      evaluator: '',
      comparison: '',
      value: true
    }
  },
  settings: {
    json: false
  }
}
