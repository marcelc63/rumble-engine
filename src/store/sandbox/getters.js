/*
export const someGetter = (state) => {
}
*/

export const char = state => {
  return state.char
}

export const chars = state => {
  return state.chars
}

export const skills = state => {
  return state.char.skills
}

export const view = state => {
  return state.char.view
}

export const charge = state => {
  return state.char.status.onCharge
}

export const arenas = state => {
  return state.arenas
}

export const arena = state => {
  return state.arena
}

export const page = state => {
  return state.page
}

export const settings = state => {
  return state.settings
}

export const conditions = state => {
  return state.conditions
}
