/*
export const someAction = (state) => {
}
*/

export const startTime = ({ commit }) => {
  setInterval(() => {
    commit('updateTime')
  }, 1000)
}
