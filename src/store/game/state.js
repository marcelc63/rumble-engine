export default {
  loaded: false,
  buffer: {
    active: false,
    caster: {},
    skill: null,
    target: {},
    turnid: ''
  },
  state: {},
  action: [],
  active: [],
  redeem: {
    g: 0,
    r: 0,
    b: 0,
    w: 0
  },
  exchange: {
    receive: '',
    offer: {
      g: 0,
      r: 0,
      b: 0,
      w: 0
    }
  },
  meta: {
    mode: 'playing', //playing, spectate, replay
    channel: 'private', //private, ladder,
    ally: 'odd',
    enemy: 'even',
    status: {
      phase: 'ongoing',
      winner: ''
    },
    profile: {
      odd: {
        displayname: 'Player 1',
        username: '',
        avatar: ''
      },
      even: {
        displayname: 'Player 2',
        username: '',
        avatar: ''
      }
    }
  },
  settings: {
    sound: true,
    skills: false,
    json: false,
    chat: false
  },
  modal: {
    redeem: false,
    exchange: false,
    finish: false,
    surrender: false
  },
  mobile: {
    description: false,
    chat: false
  },
  desc: {
    mode: 'profile', //profile, char, skills,
    char: 0, //char index
    skillId: '', //skill id
    team: '' //team -> ally or enemy
  },
  now: new Date().getTime()
}
