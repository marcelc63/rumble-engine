/*
export const someGetter = (state) => {
}
*/

export const load = state => {
  return state.loaded
}
export const state = state => {
  return state.state
}
export const turnid = state => {
  return state.state.turnid
}
export const action = state => {
  return state.action
}
export const active = state => {
  return state.active
}
export const redeem = state => {
  return state.redeem
}
export const meta = state => {
  return state.meta
}
export const buffer = state => {
  return state.buffer
}
export const exchange = state => {
  return state.exchange
}
export const settings = state => {
  return state.settings
}
export const desc = state => {
  return state.desc
}
export const all = state => {
  return state
}
export const status = state => {
  return state.state.status
}
export const winner = state => {
  return state.meta.status.winner
}
export const modal = state => {
  return state.modal
}
export const mobile = state => {
  return state.mobile
}
export const profile = state => {
  return state.meta.profile
}
