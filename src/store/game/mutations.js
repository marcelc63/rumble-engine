/*
export const someMutation = (state) => {
}
*/

export const reset = (state, payload) => {
  let reset = {
    ...state,
    loaded: false,
    buffer: {
      active: false,
      caster: {},
      skill: null,
      target: {},
      turnid: ''
    },
    state: {},
    action: [],
    active: [],
    redeem: {
      g: 0,
      r: 0,
      b: 0,
      w: 0
    },
    exchange: {
      receive: '',
      offer: {
        g: 0,
        r: 0,
        b: 0,
        w: 0
      }
    },
    meta: {
      mode: 'playing', //playing, spectate, replay
      channel: 'private', //private, ladder,
      ally: '',
      enemy: '',
      status: {
        phase: 'ongoing',
        winner: ''
      },
      profile: {
        odd: {
          displayname: 'Player 1',
          username: '',
          avatar: ''
        },
        even: {
          displayname: 'Player 2',
          username: '',
          avatar: ''
        }
      }
    },
    settings: {
      sound: true,
      skills: false,
      json: false,
      chat: false
    },
    modal: {
      redeem: false,
      exchange: false,
      finish: false,
      surrender: false
    },
    mobile: {
      description: false,
      chat: false
    },
    desc: {
      mode: 'profile', //profile, char, skills,
      char: 0, //char index
      skillId: '', //skill id
      team: '' //team -> ally or enemy
    }
  }

  Object.assign(state, reset)
}

export const load = (state, payload) => {
  state.loaded = payload
}

export const using = (state, payload) => {
  let active = payload.filter(x => x.persistence === 'instant')
  let action = payload.filter(x => x.persistence !== 'instant')
  state.active = active
  state.action = action
}

export const action = (state, payload) => {
  state.action = payload
}

export const state = (state, payload) => {
  state.state = payload

  //Match end
  if (payload.status.phase === 'finish') {
    state.meta.status = payload.status
    state.modal.finish = true
  }
}

export const meta = (state, payload) => {
  state.meta = payload
  if (payload.mode === 'spectate') {
    state.settings.skills = true
  }
}

export const exchange = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'RESET') {
    state.exchange.receive = ''
    state.exchange.offer = {
      g: 0,
      r: 0,
      b: 0,
      w: 0
    }
  }
  if (type === 'EXCHANGE') {
    state.exchange.receive = pkg.receive
    state.exchange.offer = pkg.offer
  }
}

export const redeem = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'RESET') {
    state.redeem = {
      g: 0,
      r: 0,
      b: 0,
      w: 0
    }
  }
  if (type === 'MUTATE') {
    state.redeem = pkg
  }
}

export const queue = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'QUEUE_SKILL') {
    let buffer = {
      active: true,
      caster: {
        char: pkg.char,
        charId: pkg.charId,
        team: pkg.team
      },
      skillId: pkg.skillId,
      persistence: pkg.persistence,
      target: {},
      turnid: state.state.turnid
    }
    state.buffer = buffer
  }
  if (type === 'QUEUE_REMOVE') {
    state.action = state.action.filter(
      x =>
        !(
          x.caster.char === pkg.char &&
          x.caster.team === pkg.team &&
          x.skill === pkg.skill &&
          x.turnid === pkg.turnid
        )
    )
  }
}

export const target = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'TARGET_REGISTER') {
    let buffer = {
      ...state.buffer,
      target: {
        char: pkg.char,
        charId: pkg.charId,
        team: pkg.team
      }
    }
    delete buffer.active
    state.action = state.action.concat(buffer)
  }
}

export const buffer = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'RESET') {
    state.buffer = {
      active: false,
      caster: {},
      skill: null,
      target: {},
      turnid: ''
    }
  }
}

export const desc = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'CHAT') {
    state.desc.mode = 'chat'
  }
  if (type === 'PROFILE') {
    state.desc.mode = 'profile'
    state.desc.team = pkg.team
  }
  if (type === 'CHAR') {
    state.desc.mode = 'char'
    state.desc.team = pkg.team
    state.desc.char = pkg.char
  }
  if (type === 'SKILL') {
    console.log('getter', pkg.skillId)
    state.desc.mode = 'skill'
    state.desc.team = pkg.team
    state.desc.char = pkg.char
    state.desc.skillId = pkg.skillId
  }
}

export const settings = (state, payload) => {
  let { type } = payload
  if (type === 'SKILLS') {
    state.settings.skills = !state.settings.skills
  }
  if (type === 'JSON') {
    state.settings.json = !state.settings.json
  }
  if (type === 'CHAT') {
    state.settings.chat = !state.settings.chat
  }
}

export const modal = (state, payload) => {
  let { type, value } = payload
  if (type === 'REDEEM') {
    state.modal.redeem = value
  }
  if (type === 'EXCHANGE') {
    state.modal.exchange = value
  }
  if (type === 'RESET') {
    state.modal.redeem = false
    state.modal.exchange = false
  }
  if (type === 'FINISH') {
    state.modal.finish = value
  }
  if (type === 'SURRENDER') {
    state.modal.surrender = value
  }
}

export const updateTime = state => {
  state.now = new Date().getTime() / 1000
}

export const calculating = state => {
  state.state.turn = state.state.turn + 1
}

export const mobile = (state, payload) => {
  let { type, value } = payload
  if (type === 'DESCRIPTION') {
    state.mobile.description = value
  }
  if (type === 'CHAT') {
    state.mobile.chat = value
  }
}
