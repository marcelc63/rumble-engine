import Vue from 'vue'
import Vuex from 'vuex'

import game from './game'
import builder from './builder'
import lobby from './lobby'
import chat from './chat'
import sandbox from './sandbox'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    game,
    builder,
    lobby,
    chat,
    sandbox
  }
})

export default store
