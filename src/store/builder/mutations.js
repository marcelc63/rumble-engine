/*
export const someMutation = (state) => {
}
*/

export const char = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'CHAR_CREATE') {
    state.char = pkg
  }
  if (type === 'CHAR_SAVE') {
    state.char = {
      ...pkg,
      skills: state.char.skills
    }
  }
  if (type === 'CHAR_UPDATE') {
    state.char = pkg
  }
}

export const skill = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'SKILL_ADD') {
    let char = state.char
    char.skills = char.skills.concat(pkg)
    state.char = char
  }
  if (type === 'SKILL_REMOVE') {
    let char = state.char
    char.skills = char.skills.filter((x, i) => i !== pkg.index)
    state.char = char
  }
}

export const effect = (state, payload) => {
  let { type, pkg, skill } = payload
  if (type === 'EFFECT_ADD') {
    let char = state.char
    let skills = char.skills[skill]
    skills.effects = skills.effects.concat(pkg)
    state.char = char
  } else if (type === 'EFFECT_REMOVE') {
    let char = state.char
    let skills = char.skills[skill]
    skills.effects = skills.effects.filter((x, i) => i !== pkg.index)
    state.char = char
  }
}

export const after = (state, payload) => {
  let { type, pkg, skill, effect } = payload
  if (type === 'AFTER_ADD') {
    let char = state.char
    let skills = char.skills[skill]
    let effects = char.skills[skill].effects[effect]
    effects.after = effects.after.concat(pkg)
    state.char = char
  }
}

export const evaluate = (state, payload) => {
  let { type, pkg, skill, effect } = payload
  if (type === 'EVALUATE_DELETE') {
    let char = state.char
    let skills = char.skills[skill]
    let model = pkg.model
    let child = pkg.child
    // let effects = char.skills[skill].effects[effect]
    // effects.after = effects.after.concat(pkg)
    if (child === undefined) {
      skills[model] = skills[model].filter((x, i) => i !== pkg.index)
    } else {
      skills[model][child] = skills[model][child].filter(
        (x, i) => i !== pkg.index
      )
    }
    state.char = char
  } else if (type === 'EVALUATE_EFFECT_DELETE') {
    let char = state.char
    let effects = char.skills[skill].effects[effect]
    let model = pkg.model
    // let effects = char.skills[skill].effects[effect]
    // effects.after = effects.after.concat(pkg)
    effects[model] = effects[model].filter((x, i) => i !== pkg.index)
    state.char = char
  }
}

export const view = (state, payload) => {
  let { type, val } = payload
  if (type === 'VIEW_ADD') {
    let view = state.char.view
    if (!view.includes(val) && view.length < 4) {
      view = view.concat(val)
      state.char.view = view
    }
  }
  if (type === 'VIEW_REMOVE') {
    let view = state.char.view
    view = view.filter(x => x !== val)
    state.char.view = view
  }
  if (type === 'VIEW_UP') {
    let view = state.char.view
    let from = view.findIndex(x => x === val)
    if (from > 0) {
      let to = from - 1
      view.splice(
        to, // new index,
        0, // no removal
        view.splice(from, 1)[0] // detach the item and return it
      )
      state.char.view = view
    }
  }
  if (type === 'VIEW_DOWN') {
    let view = state.char.view
    let from = view.findIndex(x => x === val)
    if (from < 4) {
      let to = from + 1
      view.splice(
        to, // new index,
        0, // no removal
        view.splice(from, 1)[0] // detach the item and return it
      )
      state.char.view = view
    }
  }
}

export const charge = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'CHARGE_ADD') {
    console.log('hi')
    state.char.status.onCharge = state.char.status.onCharge.concat(pkg)
  }
  if (type === 'CHARGE_REMOVE') {
    state.char.status.onCharge = state.char.status.onCharge.filter(
      x => x.id !== pkg.id
    )
  }
}

export const arenas = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'LOAD') {
    state.arenas = pkg.arenas
  }
}

export const arena = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'SELECT') {
    state.arena.name = pkg.arena.name
    state.arena.description = pkg.arena.description
    state.arena._id = pkg.arena._id
  }
}
