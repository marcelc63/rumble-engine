export default {
  char: {
    status: {
      onCharge: []
    }
  },
  view: {
    tab: '',
    skill: 0,
    effect: 0
  },
  arenas: [],
  arena: {
    name: '',
    description: '',
    _id: ''
  }
}
