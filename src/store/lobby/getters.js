/*
export const someGetter = (state) => {}
 */

export const teams = state => {
  return state.teams
}

export const chars = state => {
  return state.chars
}

export const team = state => {
  let teams = state.teams
  let select = state.select
  let team = teams.filter(x => x._id === select.team)[0]
  return team === undefined ? {} : team
}

export const check = state => {
  let teams = state.teams
  let select = state.select
  let check = teams.findIndex(x => x._id === select.team)
  return check !== -1 ? true : false
}

export const char = state => {
  let index = state.teams.findIndex(x => x._id === state.select.team)
  if (index !== -1) {
    let team = state.teams[index]
    let slot = state.select.slot
    let char = team[slot]
    return char !== '' ? char : false
  }
  return false
}

export const select = state => {
  return state.select
}

export const profile = state => {
  return state.profile
}

export const activities = state => {
  return state.activities
}

export const arena = state => {
  return state.arena
}