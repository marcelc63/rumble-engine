import axios from 'axios'

/*
export const someAction = (state) => {}
 */

export const teams = ({ state, commit }, payload) => {
  let { type, pkg } = payload
  if (type === 'LOAD') {
    axios.defaults.headers.common['Authorization'] =
      'JWT ' + localStorage.getItem('jwtToken')
    axios
      .get(process.env.API + '/api/team')
      .then(res => {
        let data = res.data
        commit('teams', { type: 'LOAD', pkg: { teams: data } })
      })
      .catch(e => {
        console.log('error', e)
      })
  }
  if (type === 'ADD') {
    let packet = {
      name: pkg.name,
      one: '',
      two: '',
      three: ''
    }

    axios.defaults.headers.common['Authorization'] =
      'JWT ' + localStorage.getItem('jwtToken')
    axios
      .post(process.env.API + '/api/team', packet)
      .then(res => {
        let data = res.data
        commit('teams', { type: 'ADD', pkg: { team: data } })
      })
      .catch(e => {
        console.log('error', e)
      })
  }
  if (type === 'CHAR') {
    let index = state.teams.findIndex(x => x._id === state.select.team)
    if (index !== -1) {
      let team = { ...state.teams[index] }
      let current = team[state.select.slot]
      if (team.one === pkg.char) {
        team[state.select.slot] = team.one
        team.one = current
      } else if (team.two === pkg.char) {
        team[state.select.slot] = team.two
        team.two = current
      } else if (team.three === pkg.char) {
        team[state.select.slot] = team.three
        team.three = current
      } else {
        team[state.select.slot] = pkg.char
      }

      //Commit
      commit('teams', { type: 'UPDATE', pkg: { team: team } })

      //Save
      axios.defaults.headers.common['Authorization'] =
        'JWT ' + localStorage.getItem('jwtToken')
      axios
        .post(process.env.API + '/api/team/update/' + team._id, team)
        .then(res => {
          // let data = res.data
          // commit('teams', { type: 'UPDATE', pkg: { team: data } })
        })
        .catch(e => {
          console.log('error', e)
        })
    }
  }
  if (type === 'DELETE') {
    axios.defaults.headers.common['Authorization'] =
      'JWT ' + localStorage.getItem('jwtToken')
    axios
      .post(process.env.API + '/api/team/delete/' + state.select.team)
      .then(res => {
        commit('teams', { type: 'DELETE' })
      })
      .catch(e => {
        console.log('error', e)
      })
  }
}

export const select = ({ state, commit }, payload) => {
  let { type, pkg } = payload
  if (type === 'TEAM') {
    let packet = {
      team: pkg._id
    }
    axios.defaults.headers.common['Authorization'] =
      'JWT ' + localStorage.getItem('jwtToken')
    axios
      .post(process.env.API + '/api/profile/update/team', packet)
      .then(res => {
        commit('select', { type: 'TEAM', pkg: { _id: pkg._id } })
      })
      .catch(e => {
        console.log('error', e)
      })
  }
}

export const profile = ({ state, commit }, payload) => {
  let { type } = payload
  let load = state.profile.load
  if (type === 'REFRESH') {
    load = false
  }
  if (!load) {
    axios.defaults.headers.common['Authorization'] =
      'JWT ' + localStorage.getItem('jwtToken')
    axios
      .get(process.env.API + '/api/profile')
      .then(res => {
        let data = res.data
        commit('profile', { type: 'LOAD', pkg: { profile: data } })
      })
      .catch(e => {
        console.log('error', e)
      })
  }
}

export const activities = ({ state, commit }, payload) => {
  let { type, activities } = payload
  if (type === 'SPECTATORS') {
    commit('activities', { type: 'SPECTATORS', pkg: { activities } })
    return
  }
  commit('activities', { type: 'UPDATE', pkg: { activities } })
}
