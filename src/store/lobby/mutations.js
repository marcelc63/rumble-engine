/*
export const someMutation = (state) => {}
 */

import uniqid from 'uniqid'
import { LocalStorage, SessionStorage } from 'quasar'

export const teams = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'ADD') {
    state.teams.push(pkg.team)
    state.select.team = pkg.team._id
  }
  if (type === 'UPDATE') {
    let index = state.teams.findIndex(x => x._id === state.select.team)
    if (index !== -1) {
      let team = pkg.team
      state.teams = state.teams.map(x => {
        if (x._id === state.select.team) {
          return team
        }
        return x
      })
    }
  }
  if (type === 'DELETE') {
    state.teams = state.teams.filter(x => x._id !== state.select.team)
  }
  if (type === 'LOAD') {
    state.teams = pkg.teams
  }
}

export const chars = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'LOAD') {
    state.chars = pkg.chars
  }
}

export const select = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'TEAM') {
    state.select.team = pkg._id
  }
  if (type === 'SLOT') {
    state.select.slot = pkg.slot
  }
}

export const profile = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'LOAD') {
    state.profile = { ...pkg.profile, load: true }
    state.select.team = pkg.profile.team
  }
}

export const activities = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'SPECTATORS') {
    state.activities.spectators = pkg.activities
  }
  if (type === 'UPDATE') {
    state.activities.online = pkg.activities.online
    state.activities.matches = pkg.activities.matches
  }
}

export const arena = (state, payload) => {
  let { type, pkg } = payload
  if (type === 'CHOOSE') {
    state.arena = {
      name: pkg.name,
      logo: pkg.logo,
      _id: pkg._id
    }
  }
}
