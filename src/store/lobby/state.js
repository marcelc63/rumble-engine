export default {
  teams: [],
  chars: [],
  select: {
    team: '',
    char: '',
    slot: ''
  },
  profile: {
    displayname: '',
    avatar: '',
    background: {
      ingame: '',
      lobby: '',
      profile: ''
    },
    load: false
  },
  activities: {
    online: [],
    matches: [],
    spectators: []
  },
  arena: {
    name: '',
    _id: ''
  }
}
