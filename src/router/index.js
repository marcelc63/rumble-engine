import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

const Router = new VueRouter({
  /*
   * NOTE! Change Vue Router mode from quasar.conf.js -> build -> vueRouterMode
   *
   * When going with "history" mode, please also make sure "build.publicPath"
   * is set to something other than an empty string.
   * Example: '/' instead of ''
   */

  // Leave as is and change from quasar.conf.js instead!
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE,
  scrollBehavior: () => ({ y: 0 }),
  routes
})

Router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwtToken') == null) {
      next({
        path: '/',
        params: { nextUrl: to.fullPath }
      })
    } else {
      next()
      let usergroup = localStorage.getItem('usergroup')
      if (to.matched.some(record => record.meta.is_admin)) {
        if (usergroup === 'admin') {
          next()
        } else {
          // next({ path: '/lobby' }) //Disabling for public
          next()
          // next({
          //   path: '/splash',
          //   params: { nextUrl: to.fullPath }
          // })
        }
      } else {
        // next() //Disabling for public
        if (usergroup === 'admin') {
          next()
        } else {
          // next({ path: '/lobby' })
          next()
          // next({
          //   path: '/splash',
          //   params: { nextUrl: to.fullPath }
          // })
        }
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem('jwtToken') == null) {
      next()
    } else {
      next({ path: '/arena' })
    }
  } else {
    next()
  }
})

export default Router
