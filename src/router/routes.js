export default [
  // {
  //   path: '/',
  //   component: () => import('layouts/default'),
  //   children: [{ path: '', component: () => import('pages/index') }]
  // },

  {
    path: '/game/:room',
    component: () => import('layouts/ingame'),
    children: [
      {
        path: '',
        component: () => import('pages/game/game'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/test/:room',
    component: () => import('layouts/ingame'),
    children: [
      {
        path: '',
        component: () => import('pages/game/tester'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/profile',
    component: () => import('layouts/profile'),
    children: [
      {
        path: '',
        component: () => import('pages/profile/profile'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/arena',
    component: () => import('layouts/home'),
    children: [
      {
        path: '',
        component: () => import('pages/game/arena'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/lobby',
    component: () => import('layouts/home'),
    children: [
      {
        path: '',
        component: () => import('pages/main/lobby'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'teams',
        component: () => import('pages/main/teams'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'chars',
        component: () => import('pages/main/chars'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/sandbox',
    component: () => import('layouts/sandbox'),
    children: [
      {
        path: '',
        component: () => import('pages/sandbox/dashboard'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'char',
        component: () => import('pages/sandbox/char'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'skill',
        component: () => import('pages/sandbox/skill'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'effect',
        component: () => import('pages/sandbox/effect'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/test/:room',
    component: () => import('layouts/game'),
    children: [
      {
        path: '',
        component: () => import('pages/tester'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },

  {
    path: '/profile',
    component: () => import('layouts/main'),
    children: [
      {
        path: '',
        component: () => import('pages/profile'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },

  {
    path: '/selection',
    component: () => import('layouts/main'),
    children: [
      {
        path: '',
        component: () => import('pages/selection'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },

  {
    path: '/',
    component: () => import('layouts/account'),
    children: [
      {
        path: '',
        component: () => import('pages/account/login'),
        meta: {
          guest: true
        }
      },
      {
        path: 'register',
        component: () => import('pages/account/register'),
        meta: {
          guest: true
        }
      },
      {
        path: 'splash',
        component: () => import('pages/profile/splash'),
        meta: {}
      }
    ]
  },

  {
    path: '/admin',
    component: () => import('layouts/main'),
    children: [
      {
        path: 'char',
        component: () => import('pages/builder'),
        meta: {
          requiresAuth: true,
          is_admin: true
        }
      },
      {
        path: '',
        component: () => import('pages/builder/arena'),
        meta: {
          requiresAuth: true,
          is_admin: true
        }
      }
    ]
  },

  {
    // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
