import VueSocketIO from 'vue-socket.io'

export default ({ Vue }) => {
  Vue.use(
    new VueSocketIO({
      debug: true,
      connection: process.env.API,
      options: {
        query: 'auth_token=' + localStorage.getItem('jwtToken')
      } //Optional options
    })
  )
}
