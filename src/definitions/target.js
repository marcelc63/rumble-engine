let skill = [
  'enemy',
  'all enemies',
  'self',
  'ally',
  'other ally',
  'other allies',
  'all allies',
  'self and enemy',
  'self or enemy', //tbi
  'self and ally', //tbi
  'all',
  'enemy or ally',
  'enemy and all allies',
  'random enemy',
  'random ally'
].map(x => {
  return {
    label: x,
    value: x
  }
})

let effect = [
  'target',
  'target allies',
  'target team',
  'caster',
  'caster ally', //tbt
  'caster allies',
  'caster team',
  'caster and target', //tbt
  'caster and ally', //tbt
  'enemy',
  'enemy team',
  'ally',
  'ally team',
  'random enemy', //tbi
  'random ally' //tbi
].map(x => {
  return {
    label: x,
    value: x
  }
})

export default {
  skill,
  effect
}
