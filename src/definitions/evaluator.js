export const condition = [
  {
    type: 'active',
    owner: 'effect',
    subject: 'effects',
    direction: 'direction',
    evaluator: 'bool',
    comparison: 'skills'
  },
  {
    type: 'specific',
    owner: 'effect',
    subject: 'specific',
    direction: 'direction',
    evaluator: 'bool',
    comparison: 'none'
  },
  {
    type: 'charge',
    owner: 'effect',
    subject: 'charges',
    direction: 'direction',
    evaluator: 'int',
    comparison: 'int'
  },
  {
    type: 'multiply',
    owner: 'charge',
    subject: 'charges',
    direction: 'direction',
    evaluator: 'multiply',
    comparison: 'int',
    value: 'int'
  },
  {
    type: 'mark',
    owner: 'effect',
    subject: 'effects',
    direction: 'direction',
    evaluator: 'marking',
    comparison: 'skills'
  },
  {
    type: 'hp',
    owner: 'char',
    subject: 'none',
    direction: 'direction',
    evaluator: 'int',
    comparison: 'int'
  },
  {
    type: 'multiply hp',
    owner: 'char',
    subject: 'none',
    direction: 'direction',
    evaluator: 'multiplyHp',
    comparison: 'int'
  },
  {
    type: 'usage',
    owner: 'skill',
    subject: 'skills',
    direction: 'direction',
    evaluator: 'int',
    comparison: 'int'
  },
  {
    type: 'multiply usage',
    owner: 'skill',
    subject: 'skills',
    direction: 'direction',
    evaluator: 'none',
    comparison: 'none'
  },
  {
    type: 'toggle',
    owner: 'effect',
    subject: 'effects',
    direction: 'direction',
    evaluator: 'bool',
    comparison: 'skills'
  }
]

export const evaluator = [
  { type: '>', eval: 'int' },
  { type: '<', eval: 'int' },
  { type: '>=', eval: 'int' },
  { type: '<=', eval: 'int' },
  { type: '===', eval: 'int' },
  { type: '!==', eval: 'int' },
  { type: 'exist', eval: 'bool' },
  { type: 'nonexist', eval: 'bool' }
]

//Future
export const conditionFuture = [
  { type: 'hp', owner: 'char', eval: 'int' },
  { type: 'invul', owner: 'char', eval: 'bool' },
  { type: 'cooldown', owner: 'skill', eval: 'int' },
  { type: 'usage', owner: 'skill', eval: 'int' },
  { type: 'duration', owner: 'effect', eval: 'int' },
  { type: 'stack', owner: 'effect', eval: 'int' },
  { type: 'active', owner: 'effect', eval: 'bool' },
  { type: 'charge', owner: 'effect', eval: 'int' }
]

export const conditionSchema = [
  {
    type: 'active',
    subject: 'effects',
    direction: 'direction',
    evaluator: 'bool',
    comparison: 'skills',
    value: 'input'
  }
]

export const conditionOptions = {
  direction: [
    { label: 'caster', value: 'caster' },
    { label: 'target', value: 'target' }
  ],
  bool: [{ label: 'true', value: true }, { label: 'false', value: false }],
  marking: [{ label: 'aim', value: 'aim' }, { label: 'avoid', value: 'avoid' }],
  multiply: [
    { label: 'consume all', value: 'consume all' },
    { label: 'consume some', value: 'consume some' },
    { label: 'consume none', value: 'consume none' }
  ],
  multiplyHp: [{ label: 'interval', value: 'interval' }],
  int: [
    { label: '0', value: 0 },
    { label: '1', value: 1 },
    { label: '2', value: 2 },
    { label: '3', value: 3 },
    { label: '4', value: 4 },
    { label: '5', value: 5 },
    { label: '10', value: 10 },
    { label: '15', value: 15 },
    { label: '20', value: 20 },
    { label: '25', value: 25 },
    { label: '30', value: 30 },
    { label: '35', value: 35 },
    { label: '40', value: 40 },
    { label: '45', value: 45 },
    { label: '50', value: 50 },
    { label: '55', value: 55 },
    { label: '60', value: 60 },
    { label: '65', value: 65 },
    { label: '70', value: 70 },
    { label: '75', value: 75 },
    { label: '80', value: 80 },
    { label: '85', value: 85 },
    { label: '90', value: 90 },
    { label: '95', value: 95 },
    { label: '100', value: 100 }
  ]
}
