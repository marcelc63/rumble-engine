//Define

let effectView = {
  name: '', //Inherit from Skill
  description: '',
  display: '' //Internal nickname
}

let effectMeta = {
  id: '', //Inherit from Skill
  caster: '', //Caster's id
  turnid: '' //Turn Id
}

let effectBasic = {
  type: '',
  val: [{ type: 'default', value: 0, schema: 'int' }],
  target: [{ type: 'default', value: 'target', schema: 'string' }], //Caster or target
  duration: [{ type: 'default', value: 1, schema: 'int' }], //How long it's active. Minimum of 1
  during: 'odd', //When will it trigger, odd turn or even turn
  condition: [{ type: 'default', value: true, schema: 'bool' }], //This mechanic will trigger if certain condition is fulfilled
  persistence: '', //Persistence of effect
  class: '', //Class of effect
  scope: {
    type: 'all',
    options: [],
    detail: ''
  },
  orientation: {
    type: 'none',
    options: [],
    detail: ''
  }
}

let effectWatch = {
  isWatch: false,
  watch: {
    type: '', //Activation Watch or Deactivation Watch
    condition: '',
    subject: [],
    direction: 'target',
    evaluator: '',
    comparison: '',
    option: '',
    orientation: ''
  },
  watchStore: {
    duration: 1,
    during: 'this turn'
  }
}

let effectMutable = {
  current: 0,
  usage: 0,
  stack: 1,
  charge: 0,
  multi: 0,
  active: true,
  used: false //Use this for target state persistence,
}

// let effectSuccession = {
//   isDistributable: true,
// isActivated: true
//   distribute: [] //Just the effect ID
// }

let effectState = {
  isStack: [{ type: 'default', value: false, schema: 'bool' }],
  isInvisible: [{ type: 'default', value: false, schema: 'bool' }],
  isMulti: [{ type: 'default', value: false, schema: 'bool' }],
  isUnremovable: [{ type: 'default', value: false, schema: 'bool' }],
  isHarmful: [{ type: 'default', value: false, schema: 'bool' }],
  isPiercing: [{ type: 'default', value: false, schema: 'bool' }],
  isUnpierceable: [{ type: 'default', value: false, schema: 'bool' }],
  isLastTurn: [{ type: 'default', value: false, schema: 'bool' }],
  isIgnoreInvul: [{ type: 'default', value: false, schema: 'bool' }],
  isRefresh: false,
  isCantKill: false,
  isConsolidate: false,
  isEndAtDeath: false
}

let effect = {
  ...effectView,
  ...effectBasic,
  ...effectMeta,
  ...effectMutable,
  ...effectState,
  ...effectWatch
}

export default effect

//Effects

export const types = [
  'damage',
  'invul',
  'stun',
  'buff',
  'dr',
  'dd',
  'state',
  'heal',
  'cost',
  'energy',
  'boost',
  'nerf',
  'counter',
  'reflect',
  'redirect',
  'remove',
  'end',
  'ignore',
  'disable',
  'cooldown',
  'duration',
  'transform',
  'charge',
  'stack',
  'mark',
  'allow',
  'instakill',
  'cant die'
].map(x => {
  return {
    label: x,
    value: x
  }
})

export const schema = [
  {
    type: 'energy',
    action: ['drain', 'remove', 'gain', 'steal']
  },
  {
    type: 'remove',
    action: [
      'harmful',
      'non-harmful',
      'class',
      'persistence',
      'specific',
      'all'
    ]
  },
  {
    type: 'cooldown',
    action: ['increase', 'decrease']
  },
  {
    type: 'duration',
    action: ['increase', 'decrease']
  },
  {
    type: 'redirect',
    action: ['from', 'to']
  },
  {
    type: 'stack',
    action: ['increase', 'decrease', 'remove']
  },
  {
    type: 'cost',
    action: ['green', 'blue', 'red', 'white', 'random']
  }
]
