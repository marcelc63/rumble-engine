let during = ['this turn', 'next turn', 'following turn'].map(x => {
  return {
    label: x,
    value: x
  }
})

export default during
