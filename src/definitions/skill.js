//Skill
let skillView = {
  name: '',
  description: '',
  picture: ''
}

let skillMeta = {
  id: '',
  index: 0,
  caster: '' //Caster ID
}

let skillBasic = {
  persistence: 'instant',
  class: 'physical',
  effects: [],
  target: [{ type: 'default', value: 'enemy', schema: 'string' }],
  cooldown: [{ type: 'default', value: 0, schema: 'int' }],
  cost: {
    g: [{ type: 'default', value: 0, schema: 'int' }],
    r: [{ type: 'default', value: 0, schema: 'int' }],
    b: [{ type: 'default', value: 0, schema: 'int' }],
    w: [{ type: 'default', value: 0, schema: 'int' }],
    rd: [{ type: 'default', value: 0, schema: 'int' }]
  },
  maxUsage: 0
}

let skillMutable = {
  counter: 0,
  active: true,
  store: [],
  usage: 0
}

let skillState = {
  isHarmful: [{ type: 'default', value: true, schema: 'bool' }],
  isAllowed: [{ type: 'default', value: true, schema: 'bool' }], //Condition
  isCooldown: false,
  isStore: [{ type: 'default', value: false, schema: 'bool' }],
  isIgnoreCounter: [{ type: 'default', value: false, schema: 'bool' }],
  isIgnoreReflect: [{ type: 'default', value: false, schema: 'bool' }],
  isIgnoreStun: [{ type: 'default', value: false, schema: 'bool' }],
  isIgnoreInvul: [{ type: 'default', value: false, schema: 'bool' }],
  isMarking: [{ type: 'default', value: false, schema: 'bool' }]
}

let skill = {
  ...skillView,
  ...skillMeta,
  ...skillBasic,
  ...skillMutable,
  ...skillState
}

export default skill
