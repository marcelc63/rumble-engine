let charView = {
  name: '',
  description: '',
  picture: '',
  anime: '',
  arena: '',
  credit: {
    author: '',
    pictures: '',
    coder: ''
  },
  enable: true
}

let charBasic = {
  maxHp: 100,
  category: []
}

let charMeta = {
  _id: '', //For Database
  id: '',
  status: {
    onAttack: [],
    onReceive: [],
    onSkill: [],
    onState: [],
    onCharge: []
  },
  skills: [],
  view: []
}

let charState = {
  isShowUsage: false
}

let charMutable = {
  hp: 100,
  alive: true
}

let char = {
  ...charView,
  ...charBasic,
  ...charMeta,
  ...charState,
  ...charMutable
}

export default char
