let type = ['all', 'classes', 'effects', 'skills']
let orientation = [
  'none',
  'cost',
  'energy',
  'transform',
  'cooldown',
  'cooldown self',
  'charge',
  'remove',
  'redirect',
  'end',
  'duration'
]
let options = [
  ['physical', 'mental', 'energy', 'affliction', 'strategic'],
  ['g', 'r', 'b', 'w', 'rd']
]
let detail = [
  ['is', 'is not'],
  ['steal', 'remove', 'gain'],
  ['durated', 'toggle'],
  ['increase', 'decrease', 'set'],
  ['to', 'from'],
  ['instant', 'toggle']
]

export default {
  type: type.map(x => {
    return { label: x, value: x }
  }),
  orientation: orientation.map(x => {
    return { label: x, value: x }
  }),
  options: options,
  detail: detail
}
