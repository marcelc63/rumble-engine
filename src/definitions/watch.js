let type = ['activation', 'end'] // remove deactivation for now
let orientation = {
  class: ['is', 'is not']
}
let condition = [
  'effect',
  'no effect',
  'skill',
  'new skill',
  'no skill',
  'class',
  'class is',
  'class is not',
  'death',
  'hp',
  'dd',
  'reflect'
]
let option = ['once', 'continuous', 'end at trigger']
let evaluator = ['casting', 'receive']
let direction = ['caster', 'target', 'recipient']
let dd = ['destroyed']

export default {
  type: type.map(x => {
    return { label: x, value: x }
  }),
  condition: condition.map(x => {
    return { label: x, value: x }
  }),
  orientation: {
    class: orientation.class.map(x => {
      return { label: x, value: x }
    })
  },
  option: option.map(x => {
    return { label: x, value: x }
  }),
  evaluator: evaluator.map(x => {
    return { label: x, value: x }
  }),
  direction: direction.map(x => {
    return { label: x, value: x }
  }),
  dd: dd.map(x => {
    return { label: x, value: x }
  }),
  evaluatorHp: [
    { label: '>', value: '>' },
    { label: '<', value: '<' },
    { label: '>=', value: '>=' },
    { label: '<=', value: '<=' },
    { label: '===', value: '===' },
    { label: '!==', value: '!==' }
  ],
  subjectHp: [
    { label: '0', value: 0 },
    { label: '1', value: 1 },
    { label: '2', value: 2 },
    { label: '3', value: 3 },
    { label: '4', value: 4 },
    { label: '5', value: 5 },
    { label: '10', value: 10 },
    { label: '15', value: 15 },
    { label: '20', value: 20 },
    { label: '25', value: 25 },
    { label: '30', value: 30 },
    { label: '35', value: 35 },
    { label: '40', value: 40 },
    { label: '45', value: 45 },
    { label: '50', value: 50 },
    { label: '55', value: 55 },
    { label: '60', value: 60 },
    { label: '65', value: 65 },
    { label: '70', value: 70 },
    { label: '75', value: 75 },
    { label: '80', value: 80 },
    { label: '85', value: 85 },
    { label: '90', value: 90 },
    { label: '95', value: 95 },
    { label: '100', value: 100 }
  ]
}
