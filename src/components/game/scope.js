//Simple scope function
function scope(origin, compare) {  
  //Implement for class only right now
  if (_.isObject(origin.scope)) {
    if (origin.scope.type === 'effects') {
      if (
        origin.scope.detail === 'is' &&
        origin.scope.options.includes(compare.type)
      ) {
        return true
      } else if (
        origin.scope.detail === 'is not' &&
        !origin.scope.options.includes(compare.type)
      ) {
        return true
      }
    } else if (origin.scope.type === 'classes') {
      if (
        origin.scope.detail === 'is' &&
        origin.scope.options.includes(compare.class)
      ) {
        return true
      } else if (
        origin.scope.detail === 'is not' &&
        !origin.scope.options.includes(compare.class)
      ) {
        return true
      }
    } else if (origin.scope.type === 'skills') {
      let check =
        origin.scope.options.includes(compare.id) ||
        origin.scope.options.includes(compare.parent)
      if (origin.scope.detail === 'is' && check) {
        return true
      } else if (origin.scope.detail === 'is not' && !check) {
        return true
      }
    } else if (origin.scope.type === 'all') {
      return true
    }
  }

  return false
}

export default scope
