const express = require('express')
const app = express()
const http = require('http').Server(app)
const port = 3003
var bodyParser = require('body-parser')
const history = require('connect-history-api-fallback')
var path = require('path')

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', '*')
  res.header('Access-Control-Allow-Methods', '*')
  next()
})

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

var mongoose = require('mongoose')
var credentials = require('./config.js')
mongoose.Promise = require('bluebird')
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require('bluebird'),
    useNewUrlParser: true,
    auth: { authdb: 'admin' }
  })
  .then(() => console.log('connection succesful'))
  .catch(err => console.error(err))
mongoose.set('useCreateIndex', true)

require('./app/builder.js')(app)
require('./sockets/index.js')(http)

var auth = require('./routes/auth')
app.use('/api/auth', auth)

var profile = require('./routes/profile')
app.use('/api/profile', profile)

var team = require('./routes/team')
app.use('/api/team', team)

var sandbox = require('./routes/sandbox')
app.use('/api/sandbox', sandbox)

//Basic
app.use(history())
app.use(express.static(__dirname + '/../dist/pwa-mat'))

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/../dist/pwa-mat/index.html'))
})

http.listen(port, function() {
  console.log('listening on *:' + port)
})
