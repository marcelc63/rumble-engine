const _ = require('lodash')
let evaluate = require('../../parsers/evaluate.js')
let buff = require('../buff.js')
let nerf = require('../nerf.js')
let boost = require('../boost.js')
let dr = require('../dr.js')
let dd = require('../dd.js')

// function compareTurn(origin, compare) {
//   let originTurn = parseInt(origin.turnid.replace('turn', ''))
//   let compareTurn = parseInt(compare.turnid.replace('turn', ''))
//   if (originTurn <= compareTurn) {
//     return true
//   }
//   return false
// }

async function damageResolution(pkg) {
  //Define
  let { caster, target, val, efDamage, state } = pkg
  //Logic

  //Damage Manipulation from Caster
  //Buffs
  console.log('dmg passing', efDamage.name, val)
  if (efDamage.persistence !== 'instant') {
    let getBuff = caster.status.onAttack.filter(
      x => x.type === 'buff' && x.active
    )
    for (efBuff of getBuff) {
      val = buff({
        val,
        efBuff,
        efDamage,
        state,
        caster,
        target
      })
      console.log('buff val', val)
    }
    let getNerf = caster.status.onAttack.filter(
      x => x.type === 'nerf' && x.active
    )
    console.log('check nerf', getNerf)
    for (efNerf of getNerf) {
      val = nerf({
        val,
        efNerf,
        efDamage,
        state,
        caster,
        target
      })
      console.log('nerf val', val)
    }
    //Boost
    let getBoost = target.status.onReceive.filter(
      x => x.type === 'boost' && x.active
    )
    for (efBoost of getBoost) {
      console.log('BOOST COUNT')
      // if (efDamage.persistence === 'instant') {
      //   let check = compareTurn(efDamage, efBoost) //Consider moving this check to effectAssign
      //   if (check) {
      //     continue
      //   }
      // }
      val = boost({
        val,
        efBoost,
        efDamage,
        state,
        caster,
        target
      })
    }
  }

  //Damage Manipulation from Receiver
  //Damage Reduction
  let isPiercing = evaluate({
    char: caster,
    evaluatee: efDamage.isPiercing,
    state
  })
  let getDr = target.status.onReceive.filter(x => x.type === 'dr' && x.active)
  for (efDr of getDr) {
    let isUnpierceable = evaluate({
      char: target,
      evaluatee: efDr.isUnpierceable,
      state
    })
    console.log('Unpiercieable', isUnpierceable)
    //if Piercing, skip DR
    if (isPiercing === true && isUnpierceable === false) {
      continue
    }
    if (efDamage.class === 'affliction') {
      continue
    }
    val = dr({
      val,
      efDr,
      efDamage,
      state,
      caster,
      target
    })
    console.log('PIERCING', val)
  }

  //Damage Deduction
  let getDd = target.status.onReceive.filter(x => x.type === 'dd' && x.active)
  for (efDd of getDd) {
    if (efDamage.class === 'affliction') {
      continue
    }
    val = dd({ val, efDd, efDamage, state, caster, target })
    console.log('DD', val)
  }

  console.log('dmg ending', efDamage.name, val)

  //Return
  return val
}

module.exports = damageResolution
