const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')

async function charge(pkg) {
  //Define
  let state = pkg.state
  let { effect, char } = pkg
  console.log(effect)
  let charges = char.status.onCharge
  console.log(charges)
  let detail = effect.orientation.detail
  let chargeIds = effect.orientation.options
  //Logic
  if (charges.length !== 0) {
    charges.forEach(charge => {
      if (
        chargeIds.includes(charge.id) &&
        charge.caster.id === effect.caster.charId
      ) {
        let val = evaluate(
          {
            state,
            char,
            evaluatee: effect.val,
            participant: effect.participant
          },
          'int'
        )
        if (detail === 'increase') {
          charge.amount = charge.amount + val
        }
        if (detail === 'decrease') {
          charge.amount = Math.max(charge.amount - val, 0)
        }
      }
    })
  }
  char.status.onCharge = charges
  //Return
  return state
}

module.exports = charge
