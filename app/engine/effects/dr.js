const _ = require('lodash')
let isDisable = require('../parsers/isDisable.js')
let scope = require('../parsers/scope.js')

function dr(pkg) {
  //Define
  //   let state = pkg.state;
  let { val, efDr, efDamage, caster, target } = pkg
  let valTemp = val
  //Scope Check
  let check = scope({
    origin: efDr,
    compare: efDamage
  })
  console.log('CHECK DR', check, efDr, efDamage)
  if (!check) {
    return val
  }
  //Check Disable
  let disable = isDisable({
    char: target,
    compare: efDr
  })
  console.log('DR DISABLE', disable, target)
  if (disable) {
    return val //Escape if ignore is true
  }
  val = val - efDr.current
  efDr.current = Math.max(efDr.current - valTemp, 0)
  val = Math.max(val, 0)
  //Return
  return val
}

module.exports = dr
