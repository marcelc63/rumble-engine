const _ = require('lodash')
let getChar = require('../helper/getChar.js')
let evaluate = require('../parsers/evaluate.js')
let buff = require('./buff.js')
let boost = require('./boost.js')

async function heal(pkg) {
  //Define
  let state = pkg.state
  let { effect, char } = pkg
  let caster = getChar({
    team: effect.caster.team,
    charId: effect.caster.charId,
    state
  })
  let target = char

  //Cannot heal dead char
  if (target.hp <= 0) {
    return state
  }

  let val = evaluate(
    {
      state,
      char,
      evaluatee: effect.val,
      participant: effect.participant
    },
    'int'
  ) //can have parser here ltr

  //Buff Heal
  let getBuff = caster.status.onAttack.filter(
    x => x.type === 'buff' && x.active
  )
  for (efBuff of getBuff) {
    val = buff({ val, efBuff, efDamage: effect, state })
    console.log('buff val', val)
  }

  //Heal Manipulation from Receiver
  //Boost
  let getBoost = target.status.onReceive.filter(
    x => x.type === 'boost' && x.active
  )
  for (efBoost of getBoost) {
    console.log('BOOST HEAL')
    val = boost({
      val,
      efBoost,
      efDamage: effect,
      state
    })
  }

  //Logic
  target.hp = target.hp + val
  if (target.hp > target.maxHp) {
    target.hp = target.maxHp
  }
  //Can do side effects here ltr
  //Return
  return state
}

module.exports = heal
