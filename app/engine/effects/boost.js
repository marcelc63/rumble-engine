const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let scope = require('../parsers/scope.js')

function boost(pkg) {
  //Define
  let { val, efBoost, efDamage, state } = pkg
  //Scope Check
  let check = scope({
    origin: efBoost,
    compare: efDamage
  })
  if (!check) {
    return val
  }
  //Logic
  let boostVal = evaluate(
    {
      char,
      evaluatee: efBoost.val,
      participant: efBoost.participant,
      state
    },
    'int'
  )
  let isStack = efBoost.isStack[0].value
  if (isStack) {
    boostVal = boostVal * efBoost.stack
  }
  val = val + boostVal

  console.log('BOOST', val)
  //Return
  return val
}

module.exports = boost
