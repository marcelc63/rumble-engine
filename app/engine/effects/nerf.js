const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let scope = require('../parsers/scope.js')

function nerf(pkg) {
  //Define
  let { val, efNerf, efDamage, state } = pkg
  //Scope Check
  let check = scope({
    origin: efNerf,
    compare: efDamage
  })
  if (!check) {
    return val
  }
  //Logic
  let nerfVal = evaluate(
    {
      char,
      evaluatee: efNerf.val,
      participant: efNerf.participant,
      state
    },
    'int'
  )
  let isStack = efNerf.isStack[0].value
  if (isStack) {
    nerfVal = nerfVal * efNerf.stack
  }
  val = Math.max(0, val - nerfVal)
  console.log('NERF', val)
  //Return
  return val
}

module.exports = nerf
