const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let scope = require('../parsers/scope.js')

async function remove(pkg) {
  //Define
  let state = pkg.state
  let { effect, char } = pkg
  let detail = effect.orientation.detail
  let options = effect.orientation.options
  let allStatus = _.concat(
    char.status.onAttack,
    char.status.onReceive,
    char.status.onSkill,
    char.status.onState
  ).filter(x => {
    let skillCheck = true
    // if (effect.orientation.type === 'remove') {
    //   skillCheck = options.includes(x.parent)
    // }
    let scopeCheck = scope({ origin: effect, compare: x })
    console.log('REMOVE EFFECT', skillCheck, scopeCheck, effect, x)
    if (scopeCheck) {
      console.log('REMOVE', char.name, skillCheck, scopeCheck, effect, x)
      return true
    }
    return false
  })
  console.log('REMOVE BEFORE', char.name, allStatus, options, detail)
  //Logic
  if (allStatus.length > 0) {
    allStatus.forEach(status => {
      status.active = false
      status.remove = true
    })
    console.log('REMOVE', char.name, allStatus)
  }
  //Return
  return state
}

module.exports = remove
