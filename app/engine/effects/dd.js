const _ = require('lodash')
let isDisable = require('../parsers/isDisable.js')
let scope = require('../parsers/scope.js')

function dd(pkg) {
  //Define
  //   let state = pkg.state;
  let { val, efDd, efDamage, caster, target } = pkg
  let valTemp = val
  //Scope Check
  let check = scope({
    origin: efDd,
    compare: efDamage
  })
  if (!check) {
    return val
  }
  //Check Disable
  let disable = isDisable({
    char: target,
    compare: efDd
  })
  if (disable) {
    return val //Escape if ignore is true
  }
  val = val - efDd.current
  efDd.current = Math.max(0, efDd.current - valTemp)
  val = Math.max(0, val)
  //Return
  return val
}

module.exports = dd
