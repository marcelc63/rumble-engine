const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let scope = require('../parsers/scope.js')

function buff(pkg) {
  //Define
  let { val, efBuff, efDamage, state } = pkg
  //Scope Check
  let check = scope({
    origin: efBuff,
    compare: efDamage
  })
  if (!check) {
    return val
  }
  //Logic
  //Check Stack
  let buff = evaluate(
    {
      state,
      char,
      evaluatee: efBuff.val,
      participant: efBuff.participant
    },
    'int'
  )
  console.log('BUFF UPDATE', buff, efBuff.val, efBuff.participant, val)
  if (efBuff.isStack) {
    buff = buff * efBuff.stack
  }
  val = val + buff
  console.log('after buff', val)
  //Return
  return val
}

module.exports = buff
