const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

async function remove(pkg) {
  //Define
  let state = pkg.state;
  let { effect, char } = pkg;
  //Logic
  char.alive = false;
  //Return
  return state;
}

module.exports = remove;
