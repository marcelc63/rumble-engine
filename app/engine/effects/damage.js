const _ = require('lodash')
let getChar = require('../helper/getChar.js')
let damageResolution = require('./operators/damageResolution')
let evaluate = require('../parsers/evaluate.js')

async function damage(pkg) {
  //Define
  let state = pkg.state
  let { effect, char } = pkg
  let caster = getChar({
    team: effect.caster.team,
    charId: effect.caster.charId,
    state
  })
  let target = char
  let efDamage = effect
  let val = evaluate(
    {
      state,
      char,
      evaluatee: effect.val,
      participant: effect.participant
    },
    'int'
  )
  console.log('DAMAGE val', val)
  //Logic
  val = await damageResolution({ state, caster, target, val, efDamage })
  target.hp = target.hp - val

  //Can't Die
  if (efDamage.isCantKill && target.hp <= 0) {
    target.hp = 1
  }
  let checkCantDie = target.status.onState.some(x => x.type === 'cant die')
  if (checkCantDie && target.hp <= 0) {
    target.hp = 1
  }

  //Pronoucned Dead. Can put death treshold here later
  if (target.hp <= 0) {
    target.alive = false
  }

  //Return
  return state
}

module.exports = damage
