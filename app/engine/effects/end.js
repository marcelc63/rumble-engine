const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let scope = require('../parsers/scope.js')

async function end(pkg) {
  //Define
  let state = pkg.state
  let { effect, char } = pkg
  let detail = effect.orientation.detail
  let options = effect.orientation.options

  let chars = state[ally].chars.concat(state[enemy].chars)
  let allEffects = []
  chars.forEach(char => {
    char.status.onAttack.forEach(effect => allEffects.push(effect))
    char.status.onReceive.forEach(effect => allEffects.push(effect))
    char.status.onSkill.forEach(effect => allEffects.push(effect))
    char.status.onState.forEach(effect => allEffects.push(effect))
  })

  let allStatus = allEffects.filter(x => {
    let skillCheck = true
    if (effect.orientation.type === 'end') {
      skillCheck = options.includes(x.parent)
    }
    let ownerCheck = false
    if (x.caster.id === char.id) {
      ownerCheck = true
    }
    let scopeCheck = scope({ origin: effect, compare: x })
    if (scopeCheck && skillCheck && ownerCheck) {
      console.log('END', char.name, skillCheck, scopeCheck, effect, x)
      return true
    }
    return false
  })
  console.log('END BEFORE', char.name, allStatus, options, detail)
  //Logic
  if (allStatus.length > 0) {
    allStatus.forEach(status => {
      status.active = false
      status.remove = true
    })
    console.log('END', char.name, allStatus)
  }
  //Return
  return state
}

module.exports = end
