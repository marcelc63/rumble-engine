const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

async function transform(pkg) {
  //Define
  let state = pkg.state;
  let { effect, char } = pkg;
  let duration = effect.duration;
  let from = evaluate(
    { state, char, evaluatee: effect.val, participant: effect.participant },
    "string"
  );
  let to = effect.orientation.options;
  //Logic
  let view = char.view;
  let fromIndex = view.findIndex(x => x === from);
  let toIndex = view.findIndex(x => x === to);
  console.log(from, to, fromIndex, toIndex);
  if (
    fromIndex === -1 &&
    toIndex >= 0 &&
    duration === 1 &&
    effect.orientation.detail === "durated"
  ) {
    view.splice(toIndex, 1, from);
    char.view = view;
  } else if (
    fromIndex === -1 &&
    toIndex >= 0 &&
    effect.orientation.detail === "toggle"
  ) {
    view.splice(toIndex, 1, from);
    char.view = view;
  } else if (fromIndex >= 0 && toIndex === -1) {
    view.splice(fromIndex, 1, to);
    char.view = view;
  }
  //Return
  return state;
}

module.exports = transform;
