const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let energyCost = require('../energy/energyCost.js')

async function energy(pkg) {
  //Define
  let state = pkg.state
  let { effect, char } = pkg
  let caster = effect.participant.caster.team
  let target = effect.participant.target.team
  let orientation = effect.orientation
  let val = evaluate(
    {
      state,
      char,
      evaluatee: effect.val,
      participant: effect.participant
    },
    'int'
  )
  
  let cost = {
    g: 0,
    r: 0,
    b: 0,
    w: 0,
    rd: 0
  }  

  //Logic
  if (orientation.type === 'energy') {
    let options = orientation.options
    let detail = orientation.detail

    //If Random
    let choice = _.shuffle(['g', 'r', 'b', 'w'])[0]
    if (options === 'rd') {
      options = choice
    }

    //Assign Cost
    if (options === 'g') {
      cost.g = cost.g + val
    } else if (options === 'r') {
      cost.r = cost.r + val
    } else if (options === 'b') {
      cost.b = cost.b + val
    } else if (options === 'w') {
      cost.w = cost.w + val
    }

    //Substract
    if (detail === 'remove' || detail === 'steal') {
      state[target].energy.g = Math.max(0, state[target].energy.g - cost.g)
      state[target].energy.r = Math.max(0, state[target].energy.r - cost.r)
      state[target].energy.b = Math.max(0, state[target].energy.b - cost.b)
      state[target].energy.w = Math.max(0, state[target].energy.w - cost.w)
    }
    if (detail === 'gain' || detail === 'steal') {
      state[caster].energy.g = Math.max(0, state[caster].energy.g + cost.g)
      state[caster].energy.r = Math.max(0, state[caster].energy.r + cost.r)
      state[caster].energy.b = Math.max(0, state[caster].energy.b + cost.b)
      state[caster].energy.w = Math.max(0, state[caster].energy.w + cost.w)
    }
    return state
  }
  return state
}

module.exports = energy
