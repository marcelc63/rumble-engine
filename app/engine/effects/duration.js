const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')

async function counter(pkg) {
  //Define
  let state = pkg.state
  let { effect, char } = pkg
  let skills = char.skills
  let duration = effect.duration
  let val = evaluate(
    {
      state,
      char,
      evaluatee: effect.val,
      participant: effect.participant
    },
    'int'
  )

  //Logic
  if (effect.orientation.detail === 'increase') {
    skills.forEach(skill => {
      if (skill.counter > 0) {
        skill.counter = skill.counter + val
      }
    })
  } else if (effect.orientation.detail === 'decrease') {
    skills.forEach(skill => {
      if (skill.counter > 0) {
        skill.counter = Math.max(skill.counter - val, 0)
      }
    })
  } else if (effect.orientation.detail === 'set') {
    skills.forEach(skill => {
      if (skill.counter > 0) {
        skill.counter = Math.max(val, 0)
      }
    })
  }
  //Return
  return state
}

module.exports = counter
