const _ = require('lodash')

function remove(status, turnid) {
  return status.filter(x => !(x.turnid === turnid))
}

async function cleanupPersistence(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy } = pkg
  let allyChars = state[ally].chars
  let enemyChars = state[enemy].chars
  //Logic
  let allyAlive = allyChars.filter(char => char.alive).length
  let enemyAlive = enemyChars.filter(char => char.alive).length
  if (allyAlive === 0 || enemyAlive === 0) {
    state.status = {
      phase: 'finish', //or finish
      winner: allyAlive === 0 ? enemy : ally
    }
  }
  //Return
  return state
}

module.exports = cleanupPersistence
