const _ = require('lodash')
let skillSort = require('./skillSort.js')
let evaluate = require('../parsers/evaluate.js')
let isInvul = require('../parsers/isInvul.js')
let skillCounter = require('./skillCounter')

async function applyTarget(pkg) {
  let state = pkg.state
  let chars = pkg.chars
  let skill = pkg.skill
  let item = pkg.item
  let ally = pkg.ally
  let enemy = pkg.enemy
  let { effects, picture, name, persistence, isIgnoreReflect } = skill
  let parent = skill.id

  for (effectToClone of effects) {
    //Create token to match index of random target
    let randomTarget = Math.floor(Math.random() * Math.floor(3))

    for (let { char, charId, team, i } of chars.map((x, i) => {
      return {
        char: x,
        charId: x.id,
        team: x.team,
        i
      }
    })) {
      let effect = _.cloneDeep(effectToClone)
      //Check Ignore Invul
      //Persistence Target Check
      let persistenceTargetCheck = true
      if (skill.persistence === 'instant' && item.turnid !== state.turnid) {
        persistenceTargetCheck = false
      }

      let ignoreInvul = evaluate({
        char,
        evaluatee: effect.isIgnoreInvul,
        participant: {
          caster: item.caster,
          target: item.target
        },
        state
      })

      if (!ignoreInvul) {
        //Exclude Invulnerable Characters
        if (team === enemy) {
          let invul = isInvul({
            char,
            compare: effect
          })
          console.log(
            'Skill Targeting',
            invul,
            char,
            persistenceTargetCheck,
            effect.name
          )
          if (invul && persistenceTargetCheck) {
            continue
          }
        }
      }

      //Assign Target
      let recipient = {
        charId: charId,
        team: team
      }

      //Sort
      state = await skillSort({
        ...item,
        recipient,
        picture,
        persistence,
        name,
        effect,
        parent,
        state,
        ally,
        enemy,
        random: randomTarget === i || randomTarget + 3 === i ? true : false,
        isIgnoreReflect
      })
    }
  }

  return state
}

async function skillTargeting(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { char, skill, enemy, ally, item } = pkg
  let { caster, target } = item
  let targeting = evaluate({
    char,
    evaluatee: skill.target,
    state
  })

  //Targeting -> Since effect order preceeds targeting, no need to distribute characters here
  let chars = state[ally].chars.concat(state[enemy].chars)

  //Counter
  let counterResult = await skillCounter({
    state,
    caster,
    target,
    targeting,
    ally,
    enemy,
    item
  })
  let ignoreCounter = evaluate({
    char,
    evaluatee: skill.isIgnoreCounter,
    participant: {
      caster: caster,
      target: target
    },
    state
  })
  if (counterResult.counter && item.turnid === state.turnid && !ignoreCounter) {
    return counterResult.state
  }

  //Target
  state = await applyTarget({
    state,
    chars,
    skill,
    targeting,
    item,
    ally,
    enemy
  })

  return state
}

module.exports = skillTargeting
