const _ = require('lodash')
let damage = require('../effects/damage.js')
let heal = require('../effects/heal.js')
let energy = require('../effects/energy.js')
let transform = require('../effects/transform.js')
let duration = require('../effects/duration.js')
let charge = require('../effects/charge.js')
let remove = require('../effects/remove.js')
let end = require('../effects/end.js')
let instakill = require('../effects/instakill.js')

let effectWatch = require('./effectWatch.js')
let effectPersistence = require('./effectPersistence.js')
let getChar = require('../helper/getChar.js')

async function effectApply(pkg) {
  let { state, char, status, turnid, parent, effectid, ally, enemy } = pkg
  let turn = state.turn % 2 === 0 ? 'even' : 'odd'
  let effects = status.filter(
    x =>
      x.id === effectid &&
      x.turnid === turnid &&
      x.caster.charId === pkg.caster.charId &&
      x.caster.team === pkg.caster.team &&
      x.parent === parent
  )

  console.log('before apply', effects)

  //Logic
  for (effect of effects) {
    //Escape if Caster is Dead and Persistence is not Instant
    let caster = getChar({
      team: effect.caster.team,
      charId: effect.caster.charId,
      state
    })

    if (!caster.alive) {
      if (
        !(effect.persistence === 'instant' && effect.turnid !== state.turnid)
      ) {
        //Will allow multi turn instant skill to still continue
        continue
      }
      if (effect.persistence === 'instant' && effect.turnid === state.turnid) {
        //Instant skill casted during the turn will not pass when caster is dead
        continue
      }
    }

    // //Watch and Activate
    // effect = await effectWatch({ state, char, effect, ally, enemy })

    //Escape if not Active
    if (effect.isWatch && effect.watch.type === 'activation') {
      continue
    }

    //Activate Effect
    if (effect.caster.team === turn) {
      //Action Persistence Check
      let check = await effectPersistence({ effect, caster, state })
      if (!check) {
        effect.active = true
      } else {
        effect.active = false
      }
    }

    // //For Following Turn - Skip first turn
    if (effect.turnid === state.turnid && effect.isFollowingTurn) {
      console.log(
        'skip',
        effect,
        effect.name,
        effect.turnid,
        effect.isFollowingTurn
      )
      continue
    }

    //Check Duration
    if (!(effect.during === turn && effect.active)) {
      continue
    }

    if (effect.used) {
      continue
    }

    //Apply
    if (effect.type === 'energy') {
      state = await energy({ state, char, effect })
    } else if (effect.type === 'damage') {
      state = await damage({ state, char, effect })
      effect.used = true
    } else if (effect.type === 'heal') {
      state = await heal({ state, char, effect })
    } else if (effect.type === 'transform') {
      state = await transform({ state, char, effect })
    } else if (effect.type === 'charge') {
      state = await charge({ state, char, effect })
    } else if (effect.type === 'remove') {
      state = await remove({ state, char, effect })
    } else if (effect.type === 'end') {
      state = await end({ state, char, effect })
    } else if (effect.type === 'instakill') {
      state = await instakill({ state, char, effect })
    } else if (effect.type === 'duration') {
      state = await duration({ state, char, effect })
    }
  }

  return state
}

module.exports = effectApply
