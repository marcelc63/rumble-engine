const _ = require('lodash')
let getChar = require('../helper/getChar.js')
let evaluate = require('../parsers/evaluate.js')
let effectAssign = require('./effectAssign.js')
let effectApply = require('./effectApply.js')
let skillReflect = require('./skillReflect')
let skillRedirect = require('./skillRedirect')

async function skillSort(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let {
    caster,
    target,
    recipient,
    turnid,
    effect,
    ally,
    enemy,
    random,
    isIgnoreReflect
  } = pkg

  //Logic
  //Definition
  let char = getChar({
    team: recipient.team,
    charId: recipient.charId,
    state
  })
  let targeting = evaluate({
    char,
    evaluatee: effect.target,
    participant: { caster, target: recipient },
    state
  })
  let status = char.status

  //Reflect Skill Check
  let ignoreReflect = evaluate({
    char,
    evaluatee: isIgnoreReflect,
    participant: { caster, target: recipient },
    state
  })
  if (state.turnid === turnid && !ignoreReflect) {
    targeting = await skillReflect({
      targeting,
      target,
      recipient,
      effect,
      ally,
      enemy,
      caster,
      state
    })
  }

  //Check targeting
  let isTarget = false
  if (targeting === 'target') {
    if (char.id === target.charId && char.team === target.team) {
      isTarget = true
    }
  }
  if (targeting === 'target allies') {
    if (char.team === target.team && char.id !== target.charId) {
      isTarget = true
    }
  }
  if (targeting === 'target team') {
    if (char.team === target.team) {
      isTarget = true
    }
  }
  if (targeting === 'caster') {
    if (char.id === caster.charId && char.team === caster.team) {
      isTarget = true
    }
  }
  if (targeting === 'caster ally') {
    if (
      char.team === caster.team &&
      char.id === target.charId &&
      char.id !== caster.charId
    ) {
      isTarget = true
    }
  }
  if (targeting === 'caster allies') {
    if (char.team === caster.team && char.id !== caster.charId) {
      isTarget = true
    }
  }
  if (targeting === 'caster team') {
    if (char.team === caster.team) {
      isTarget = true
    }
  }
  if (targeting === 'caster and ally') {
    if (char.id === caster.charId || char.id === target.charId) {
      isTarget = true
    }
  }
  if (targeting === 'caster and ally') {
    if (
      (char.id === caster.charId || char.id === target.charId) &&
      char.team === caster.team
    ) {
      isTarget = true
    }
  }
  if (targeting === 'enemy') {
    if (char.id === target.charId && char.team !== caster.team) {
      isTarget = true
    }
  }
  if (targeting === 'enemy team') {
    if (char.team !== caster.team) {
      isTarget = true
    }
  }
  if (targeting === 'ally') {
    if (char.id === target.charId && char.team === caster.team) {
      isTarget = true
    }
  }
  if (targeting === 'ally team') {
    if (char.team === caster.team) {
      isTarget = true
    }
  }
  if (targeting === 'random enemy') {
    if (char.team !== caster.team && random) {
      isTarget = true
    }
  }
  if (targeting === 'random ally') {
    if (char.team === caster.team && random) {
      isTarget = true
    }
  }

  if (!isTarget) {
    return state
  }

  if (state.turnid === turnid) {
    //Check condition
    let condition = evaluate(
      {
        char,
        evaluatee: effect.condition,
        participant: {
          caster,
          target: recipient
        },
        state
      },
      'bool'
    )
    if (!condition) {
      return state
    }

    //Redirect here
    skillRedirect({ recipient, state, ally, enemy, effect }, res => {
      if (res.check) {
        char = getChar({
          team: res.recipient.team,
          charId: res.recipient.charId,
          state
        })
        status = char.status
      }
    })
  }

  //Define
  let payload = {
    ...pkg,
    effect,
    char,
    state,
    recipient,
    turnid,
    effectid: effect.id
  }

  if (['dr', 'dd', 'boost'].includes(effect.type)) {
    status.onReceive = await effectAssign({
      status: status.onReceive,
      ...payload
    })
    //Apply
    state = await effectApply({
      status: status.onReceive,
      ...payload
    })
  }
  if (['buff', 'nerf'].includes(effect.type)) {
    status.onAttack = await effectAssign({
      status: status.onAttack,
      ...payload
    })
    //Apply
    state = await effectApply({
      status: status.onAttack,
      ...payload
    })
  }
  if (
    [
      'damage',
      'heal',
      'energy',
      'transform',
      'charge',
      'remove',
      'end',
      'instakill'
    ].includes(effect.type)
  ) {
    status.onSkill = await effectAssign({
      status: status.onSkill,
      ...payload
    })
    //Apply
    state = await effectApply({
      status: status.onSkill,
      ...payload
    })
  }
  if (
    [
      'allow',
      'cost',
      'invul',
      'stun',
      'state',
      'ignore',
      'disable',
      'mark',
      'cooldown',
      'duration',
      'counter',
      'redirect',
      'reflect',
      'cant die'
    ].includes(effect.type)
  ) {
    status.onState = await effectAssign({
      status: status.onState,
      ...payload
    })
    //Apply
    state = await effectApply({
      status: status.onState,
      ...payload
    })
    console.log('AFTER INVUL', state)
  }

  //Return
  return state
}

module.exports = skillSort
