const _ = require('lodash')
let getChar = require('../helper/getChar.js')
let scope = require('../parsers/scope.js')

async function skillRedirect(pkg, cb) {
  //Define
  let { recipient, ally, enemy, state, effect } = pkg

  //Escape Remove from caster
  let checkAlly = ally === recipient.team //Some effects may get side effects from this. Will need to check.
  if (effect.type === 'remove') {
    cb({ check: false })
    return
  }

  //Assign
  let char = getChar({
    team: recipient.team,
    charId: recipient.charId,
    state
  })
  let onState = char.status.onState
  //Can do skill thing inside here
  let onRedirect = onState.filter(
    x =>
      x.type === 'redirect' &&
      x.orientation.type === 'redirect' &&
      x.orientation.detail === 'from'
  )

  //Logic
  //Counter on Attack
  if (onRedirect.length > 0) {
    let redirect = onRedirect[0] //Currently only recognize one redirect
    //Add Scope Here
    let checkScope = scope({ origin: redirect, compare: effect })
    if (!checkScope) {
      cb({ check: false })
      return
    }

    //Find RedirectTo
    let allChars = state[ally].chars.concat(state[enemy].chars)
    let allEffects = []
    allChars.forEach(char => {
      char.status.onState.forEach(effect => allEffects.push(effect))
    })
    let skillId = redirect.parent
    let onTarget = allEffects.filter(
      x =>
        x.type === 'redirect' &&
        x.orientation.type === 'redirect' &&
        x.orientation.detail === 'to' &&
        x.parent === skillId
    )
    if (onTarget.length > 0) {
      let target = onTarget[0]
      cb({
        check: true,
        recipient: target.recipient
      })
      return
    }
  }

  //Return
  cb({ check: false })
  return
}

module.exports = skillRedirect
