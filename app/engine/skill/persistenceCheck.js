const _ = require('lodash')
let isInvul = require('../parsers/isInvul.js')
let getSkill = require('../helper/getSkill.js')

function remove(slot, enemy, status) {
  let result = status[slot].filter(
    x => !(x.persistence === 'control' && x.caster.team === enemy)
  )
  return result
}

async function persistenceCheck(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy, item } = pkg
  let chars = state[ally].chars.concat(state[enemy].chars)
  //Logic
  for (let char of chars) {
    //Persistence Check
    //Target State Check
    let status = char.status
    let skill = getSkill({
      team: item.caster.team,
      charId: item.caster.charId,
      skillId: item.skillId,
      state
    })
    let invul = isInvul({
      char,
      compare: skill
    })
    if (invul) {
      let effects = _.concat(
        status.onAttack,
        status.onReceive,
        status.onSkill,
        status.onState
      )
      //Deactivate Mark Action Skill
      let effectsAction = effects.filter(
        x => x.persistence === 'action' && x.caster.team !== char.team
      )
      for (let effect of effectsAction) {
        effect.active = false
      }
      //Remove Control Skill
      status.onAttack = remove('onAttack', enemy, status)
      status.onReceive = remove('onReceive', enemy, status)
      status.onSkill = remove('onSkill', enemy, status)
      status.onState = remove('onState', enemy, status)
    }
  }
  //Return
  return state
}

module.exports = persistenceCheck
