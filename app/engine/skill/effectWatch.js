const _ = require('lodash')
let getChar = require('../helper/getChar.js')

async function effectWatch(pkg) {
  let { state, effect, ally, enemy } = pkg

  if (effect.type !== 'damage') {
    return effect
  }

  console.log('EFFECT WATCH BEFORE', effect)

  //Escape if not Watch and give active true
  if (!effect.isWatch) {
    return effect
  }

  // Define
  let { type, condition, subject, evaluator, direction } = effect.watch
  let turn = state.turn % 2 === 0 ? 'even' : 'odd'
  let chars = state[ally].chars.concat(state[enemy].chars)
  let check = false
  let char = {}

  //Effects
  let allEffects = []
  chars.forEach(x => {
    x.status.onAttack.forEach(x => allEffects.push(x))
    x.status.onReceive.forEach(x => allEffects.push(x))
    x.status.onSkill.forEach(x => allEffects.push(x))
    x.status.onState.forEach(x => allEffects.push(x))
  })
  console.log(allEffects)

  //Direction
  if (direction === 'target') {
    let target = getChar({
      team: effect.recipient.team,
      charId: effect.recipient.charId,
      state
    })
    char = target
  } else if (direction === 'caster') {
    let caster = getChar({
      team: effect.caster.team,
      charId: effect.caster.charId,
      state
    })
    char = caster
  }

  //Condition
  if (condition === 'effect') {
    if (evaluator === 'casting') {
      let charEffects = allEffects.filter(
        x => x.caster.charId === char.id && x.caster.team === char.team
      )
      check = charEffects.some(x => subject.includes(x.type))
    }
    if (evaluator === 'receive') {
      let charEffects = _.concat(
        char.status.onSkill,
        char.status.onState,
        char.status.onAttack,
        char.status.onReceive
      )
      check = charEffects.some(x => subject.includes(x.type))
    }
    console.log('skill watch', type, condition, subject, evaluator)
    console.log('check', check)
  }

  if (condition === 'skill') {
    check = allEffects.some(x => {
      console.log(x.parent)
      return subject.includes(x.parent) && x.turnid === state.turnid
    })
    console.log('test check', check, subject, state.turnid)
    // if (check > -1) {
    //   check = true
    // }
  }

  //Type Execute
  if (check) {
    if (type === 'activation') {
      effect.active = true
      effect.duration = effect.watchStore.duration
      effect.during = effect.watchStore.during
      effect.isWatch = false
      console.log(effect.duration, effect.during, effect.active, subject)
    }
    if (type === 'deactivation') {
      // effect.active = false;
      // effect.remove = true;
      effect.duration = effect.watchStore.duration
      effect.during = effect.watchStore.during
      effect.isWatch = false
    }
    if (type === 'end') {
      console.log('ENDD')
      // effect.active = false;
      // effect.remove = true;
      effect.duration = 1
      effect.during = effect.watchStore.during
      effect.isWatch = false
    }
    console.log(effect)
  }

  return effect
}

module.exports = effectWatch
