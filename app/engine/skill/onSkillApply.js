const _ = require('lodash')
let damage = require('../effects/damage.js')
let heal = require('../effects/heal.js')
let energy = require('../effects/energy.js')
let transform = require('../effects/transform.js')
let duration = require('../effects/duration.js')
let charge = require('../effects/charge.js')
let remove = require('../effects/remove.js')
let end = require('../effects/end.js')
let instakill = require('../effects/instakill.js')

async function apply(pkg) {
  let { state, char, caster } = pkg
  let turn = state.turn % 2 === 0 ? 'even' : 'odd'
  let effects = char.status.onSkill.filter(
    x =>
      x.turnid === pkg.turnid &&
      x.caster.charId === pkg.caster.charId &&
      x.caster.team === pkg.caster.team &&
      x.active === true
  )
  //Logic
  for (effect of effects) {
    //Check Duration
    if (effect.during !== turn) {
      continue
    }
    //Apply
    if (effect.type === 'energy') {
      state = await energy({ state, char, effect })
    }
    if (effect.type === 'damage') {
      state = await damage({ state, char, effect })
    }
    if (effect.type === 'heal') {
      state = await heal({ state, char, effect })
    }
    if (effect.type === 'transform') {
      state = await transform({ state, char, effect })
    }
    if (effect.type === 'duration') {
      state = await duration({ state, char, effect })
    }
    if (effect.type === 'charge') {
      state = await charge({ state, char, effect })
    }
    if (effect.type === 'remove') {
      state = await remove({ state, char, effect })
    }
    if (effect.type === 'end') {
      state = await end({ state, char, effect })
    }
    if (effect.type === 'instakill') {
      state = await instakill({ state, char, effect })
    }
  }
  return state
}

async function onSkillApply(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy, item } = pkg
  let { caster, turnid } = item
  let chars = state[ally].chars.concat(state[enemy].chars)
  for (char of chars) {
    //apply other skills
    state = await apply({ state, char, turnid, caster })
  }
  //Return
  return state
}

module.exports = onSkillApply
