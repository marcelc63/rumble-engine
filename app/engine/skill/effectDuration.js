const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let getChar = require('../helper/getChar.js')
let scope = require('../parsers/scope.js')

async function effectDuration(payload) {
  //Define
  let { effect, pkg } = payload
  console.log(effect)
  let { caster, target, state } = pkg
  let participant = {
    //Resolving target in evaluate
    caster,
    target
  }
  let casterChar = getChar({
    team: caster.team,
    charId: caster.charId,
    state
  })

  let getDuration = casterChar.status.onState.filter(
    x => x.type === 'duration' && x.active
  )
  console.log(getDuration)
  for (effDuration of getDuration) {
    let check = scope({
      origin: effDuration,
      compare: effect
    })
    if (!check) {
      continue
    }

    let val = evaluate(
      {
        casterChar,
        evaluatee: effDuration.val,
        participant,
        state
      },
      'int'
    )
    if (effDuration.isStack) {
      val = val * effDuration.stack
    }

    effect.duration = effect.duration + val
  }

  console.log('AFTER DURATION', effect)

  return effect
}

module.exports = effectDuration
