const _ = require('lodash')
let getChar = require('../helper/getChar.js')
let getSkill = require('../helper/getSkill.js')
let scope = require('../parsers/scope.js')

function scopeCheck(pkg) {
  let { item, counter, state } = pkg
  //Check Scope
  let skill = getSkill({
    team: item.caster.team,
    charId: item.caster.charId,
    skillId: item.skillId,
    state
  })
  for (effect of skill.effects) {
    let check = scope({
      origin: counter,
      compare: effect
    })
    if (!check) {
      return false
    } else {
      return true
    }
  }
}

async function skillCounter(pkg) {
  //Define
  let state = pkg.state
  let { targeting, caster, target, ally, enemy, item } = pkg
  let { turnid } = item
  //Check
  if (state.turnid !== turnid) {
    return state
  }

  //Check Counter
  let chars = [
    getChar({
      team: caster.team,
      charId: caster.charId,
      state
    }),
    getChar({
      team: target.team,
      charId: target.charId,
      state
    })
  ]
  if (targeting === 'all enemies') {
    chars = state[ally].chars.concat(state[enemy].chars)
  } else if (targeting === 'all' && target.team === enemy) {
    chars = state[ally].chars.concat(state[enemy].chars)
  }

  //Assign
  let onState = []
  chars.forEach(char => {
    char.status.onState.forEach(effect => onState.push(effect))
  })
  //Can do skill thing inside here
  let onCounter = onState.filter(
    x =>
      x.type === 'counter' &&
      x.participant.caster.team === enemy &&
      scopeCheck({ counter: x, item, state })
  )
  // console.log("COUNTER BEGINS", onState, onCounter);

  //Logic
  //Counter on Attack
  if (onCounter.length > 0) {
    //Give them Counter Message
    onCounter[0].type = 'countered'
    onCounter[0].during = enemy
    onCounter[0].isInvisible = false
    //Return
    return {
      state,
      counter: true
    }
  }

  //Return
  return { state, counter: false }
}

module.exports = skillCounter
