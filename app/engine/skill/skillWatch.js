const _ = require('lodash')
let getChar = require('../helper/getChar.js')

//Utilities Functions
function compareTurn(origin, compare) {
  let originTurn = parseInt(origin.turnid.replace('turn', ''))
  let compareTurn = parseInt(compare.turnid.replace('turn', ''))
  if (originTurn <= compareTurn) {
    return true
  }
  return false
}

function subjectCheck(subject, compare, orientation) {
  if (orientation === '' || orientation === 'is') {
    return subject.includes(compare)
  }
  if (orientation === 'is not') {
    return !subject.includes(compare)
  }
}

async function check(pkg) {
  let { state, chars, allEffects, lastQueue } = pkg
  let turn = state.turn % 2 === 0 ? 'even' : 'odd'
  //End at Trigger Mechanic
  let trigger = false
  let triggerSkill = []

  for (char of chars) {
    console.log('CHECKING', char.name, char.team)
    let effects = _.concat(
      char.status.onSkill,
      char.status.onState,
      char.status.onAttack,
      char.status.onReceive
    ).filter(x => x.isWatch === true)

    //Logic
    for (effect of effects) {
      //Define
      let {
        type,
        condition,
        subject,
        evaluator,
        direction,
        option,
        orientation
      } = effect.watch
      let check = false

      // if (effect.during !== turn) {
      //   continue
      // }
      // Not sure what this is for :/

      // Direction
      let charToCheck = _.cloneDeep(char)
      if (direction === 'caster') {
        let casterChar = getChar({
          team: effect.caster.team,
          charId: effect.caster.charId,
          state
        })
        charToCheck = _.cloneDeep(casterChar)
      } else if (direction === 'target') {
        let targetChar = getChar({
          team: effect.target.team,
          charId: effect.target.charId,
          state
        })
        charToCheck = _.cloneDeep(targetChar)
      } else if (direction === 'recipient') {
        let targetChar = getChar({
          team: effect.recipient.team,
          charId: effect.recipient.charId,
          state
        })
        charToCheck = _.cloneDeep(targetChar)
      }
      console.log('DIRECTION CHECK', direction, charToCheck, effect)

      //Condition
      if (condition === 'effect') {
        console.log('WATCH EFFECT BEFORE', effect)
        if (evaluator === 'casting') {
          let charEffects = allEffects.filter(
            x =>
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team
          )
          check = charEffects.some(
            x => subject.includes(x.type) && compareTurn(effect, x)
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          let testEff = charEffects.filter(
            x => subject.includes(x.type) && compareTurn(effect, x) && x.active
          )
          check = testEff.length !== 0
          console.log('WARCH EFFECT AFTER', check, effect.name, testEff)
        }
      }

      if (condition === 'no effect') {
        let newSkill
        if (evaluator === 'casting') {
          newSkill = allEffects.filter(
            x =>
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team &&
              subject.includes(x.type) &&
              // x.turnid === state.turnid &&
              compareTurn(effect, x)
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          newSkill = charEffects.filter(
            x =>
              effect.caster.charId !== x.caster.charId &&
              subject.includes(x.type) &&
              compareTurn(effect, x)
          )
        }
        check = newSkill.length === 0 ? true : false
        console.log(
          'NO NEW SKILL CHECK',
          check,
          charToCheck.name,
          state.turnid,
          newSkill
        )
      }

      if (condition === 'skill') {
        if (evaluator === 'casting') {
          check = allEffects.some(
            x =>
              subject.includes(x.parent) &&
              // x.turnid === state.turnid &&
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team &&
              x.turnid === state.turnid
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          check = charEffects.some(
            x => subject.includes(x.parent) && compareTurn(effect, x)
          )
        }
        console.log(
          'skill check',
          effect,
          check,
          subject,
          evaluator,
          state.turnid
        )
      }

      if (condition === 'new skill') {
        if (evaluator === 'casting') {
          check = allEffects.some(
            x =>
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team &&
              x.turnid === state.turnid
            // compareTurn(effect, x)
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          check = charEffects.some(x => x.turnid === state.turnid)
        }
        console.log('NEW SKILL CHECK', check, charToCheck.name, state.turnid)
      }

      if (condition === 'no skill') {
        let newSkill
        if (evaluator === 'casting') {
          newSkill = allEffects.filter(
            x =>
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team &&
              x.turnid === state.turnid
            // compareTurn(effect, x)
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          newSkill = charEffects.filter(
            x =>
              effect.caster.charId !== x.caster.charId && compareTurn(effect, x)
          )
        }
        check = newSkill.length === 0 && lastQueue ? true : false
        console.log(
          'NO NEW SKILL CHECK',
          check,
          charToCheck.name,
          state.turnid,
          allEffects,
          newSkill
        )
      }

      if (condition === 'class') {
        console.log('WATCH CLASS', effect)

        if (evaluator === 'casting') {
          let charEffects = allEffects.filter(
            x =>
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team
          )
          check = charEffects.some(
            x =>
              subjectCheck(subject, x.class, orientation) &&
              x.turnid === state.turnid
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          check = charEffects.some(
            x =>
              subjectCheck(subject, x.class, orientation) &&
              x.turnid === state.turnid
          )
        }

        console.log('test class', check, subject, orientation)
      }

      if (condition === 'class is') {
        console.log('WATCH CLASS', effect)

        if (evaluator === 'casting') {
          let charEffects = allEffects.filter(
            x =>
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team
          )
          check = charEffects.some(
            x =>
              subject.includes(x.class) &&
              x.type === orientation &&
              x.turnid === state.turnid
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          check = charEffects.some(
            x =>
              subject.includes(x.class) &&
              x.type === orientation &&
              x.turnid === state.turnid
          )
        }

        console.log('test class', check, subject, orientation)
      }

      if (condition === 'class is not') {
        console.log('WATCH CLASS', effect)

        if (evaluator === 'casting') {
          let charEffects = allEffects.filter(
            x =>
              x.caster.charId === charToCheck.id &&
              x.caster.team === charToCheck.team
          )
          check = charEffects.some(
            x =>
              !subject.includes(x.class) &&
              x.type === orientation &&
              x.turnid === state.turnid
          )
        }
        if (evaluator === 'receive') {
          let charEffects = _.concat(
            charToCheck.status.onSkill,
            charToCheck.status.onState,
            charToCheck.status.onAttack,
            charToCheck.status.onReceive
          )
          check = charEffects.some(
            x =>
              !subject.includes(x.class) &&
              x.type === orientation &&
              x.turnid === state.turnid
          )
        }

        console.log('test class', check, subject, orientation)
      }

      if (condition === 'death') {
        let alive = charToCheck.alive
        let charHp = charToCheck.hp

        if (!alive && charHp <= 0) {
          check = true
        }
        console.log('test check', check, subject, state.turnid)
      }

      if (condition === 'hp') {
        let charHp = charToCheck.hp
        let comparison = subject[0]

        if (evaluator === '>' && charHp > comparison) {
          check = true
        }
        if (evaluator === '<' && charHp < comparison) {
          check = true
        }
        if (evaluator === '>=' && charHp >= comparison) {
          check = true
        }
        if (evaluator === '<=' && charHp <= comparison) {
          check = true
        }
        if (evaluator === '===' && charHp === comparison) {
          check = true
        }
        if (evaluator === '!==' && charHp !== comparison) {
          check = true
        }
      }

      if (condition === 'dd') {
        let charEffects = _.concat(
          charToCheck.status.onSkill,
          charToCheck.status.onState,
          charToCheck.status.onAttack,
          charToCheck.status.onReceive
        )
        check = charEffects.some(
          x =>
            subject.includes(x.parent) &&
            x.caster.charId === effect.caster.charId &&
            x.caster.team === effect.caster.team &&
            x.current <= 0 &&
            // compareTurn(effect, x) &&
            x.type === 'dd'
        )

        console.log('DD SKILL CHECK', check, charToCheck.name, state.turnid)
      }

      if (condition === 'reflect') {
        console.log('WATCH REFLECT BEFORE', effect)
        let charEffects = charToCheck.status.onState.filter(
          x => x.type === 'reflect' && x.used === true
        )
        check = charEffects.length !== 0
        console.log('WARCH REFLECT AFTER', check, effect.name, charEffects)
      }

      //Type Execute
      if (check) {
        console.log('WATCH ACTIVATE', effect, type)
        triggerSkill.push(effect.parent + effect.caster.team)
        if (type === 'activation') {
          // trigger = true
          effect.active = true
          if (option !== 'continuous') {
            effect.duration = effect.watchStore.duration
            effect.during = effect.watchStore.during
          }
          effect.isWatch = false
          console.log(effect.duration, effect.during, effect.active, subject)
        }
        if (type === 'deactivation') {
          // effect.active = false
          // effect.remove = true
          effect.duration = effect.watchStore.duration
          effect.during = effect.watchStore.during
          effect.isWatch = false
        }
        if (type === 'end') {
          console.log('ENDD')
          effect.active = false
          effect.remove = true
          effect.duration = 1
          effect.during = turn
          effect.isWatch = false
        }
        console.log(effect)
      }
    }
  }

  //End at Trigger
  for (char of chars) {
    let effects = _.concat(
      char.status.onSkill,
      char.status.onState,
      char.status.onAttack,
      char.status.onReceive
    ).filter(x => x.isWatch === true)

    for (effect of effects) {
      if (
        effect.watch.option === 'end at trigger' &&
        triggerSkill.includes(effect.parent + effect.caster.team)
      ) {
        effect.active = false
        effect.remove = true
        effect.duration = 1
        effect.isWatch = false
      }
    }
  }

  return state
}

async function skillWatch(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy, item, lastQueue } = pkg
  let { turnid } = item
  let chars = state[ally].chars.concat(state[enemy].chars)
  let allEffects = []
  chars.forEach(char => {
    char.status.onAttack.forEach(effect => allEffects.push(effect))
    char.status.onReceive.forEach(effect => allEffects.push(effect))
    char.status.onSkill.forEach(effect => allEffects.push(effect))
    char.status.onState.forEach(effect => allEffects.push(effect))
  })
  console.log(chars)
  console.log(allEffects)
  // for (char of chars) {
  //apply other skills
  state = await check({
    state,
    chars,
    turnid,
    allEffects,
    lastQueue
  })
  // }
  //Return
  return state
}

module.exports = skillWatch
