const _ = require("lodash");
let getChar = require("../helper/getChar.js");
let getSkill = require("../helper/getSkill.js");

async function skillUsage(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { caster, turnid, skillId } = pkg.item;
  //Check
  if (state.turnid !== turnid) {
    return state;
  }
  //Assign
  let char = getChar({
    team: caster.team,
    charId: caster.charId,
    state
  });
  let skill = getSkill({
    team: caster.team,
    charId: caster.charId,
    skillId: skillId,
    state
  });
  //Logic
  skill.usage = skill.usage + 1;
  //Return
  return state;
}

module.exports = skillUsage;
