const _ = require("lodash");
let getChar = require("../helper/getChar.js");
let getSkill = require("../helper/getSkill.js");
let getCooldown = require("../parsers/cooldown.js");

async function skillCooldown(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { caster, turnid, skillId } = pkg.item;
  //Check
  if (state.turnid !== turnid) {
    return state;
  }
  //Assign
  let char = getChar({
    team: caster.team,
    charId: caster.charId,
    state
  });
  let skill = getSkill({
    team: caster.team,
    charId: caster.charId,
    skillId: skillId,
    state
  });
  let cooldown = getCooldown({ state, char, skill });
  //Check for Cooldown Modifier
  //Logic
  skill.counter = cooldown;
  //Return
  return state;
}

module.exports = skillCooldown;
