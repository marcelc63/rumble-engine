const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let getChar = require('../helper/getChar.js')
let buff = require('../effects/buff.js')
let nerf = require('../effects/nerf.js')
let boost = require('../effects/boost.js')

async function effectInstant(payload) {
  //Define
  let { effect, pkg } = payload
  console.log(effect)
  let { caster, target, state, char, participant } = pkg
  let valType = effect.type === 'transform' ? 'string' : 'int'
  //Logic

  if (effect.type === 'damage') {
    let val = evaluate(
      {
        char,
        evaluatee: effect.val,
        participant,
        state
      },
      valType
    )

    let casterChar = getChar({
      team: caster.team,
      charId: caster.charId,
      state
    })
    //Damage Manipulation from Caster
    //Buffs
    console.log('passing', val)
    let getBuff = casterChar.status.onAttack.filter(
      x => x.type === 'buff' && x.active
    )
    for (efBuff of getBuff) {
      val = buff({
        val,
        efBuff,
        efDamage: effect,
        state,
        caster,
        target
      })
      console.log('buff val', val)
    }
    let getNerf = casterChar.status.onAttack.filter(
      x => x.type === 'nerf' && x.active
    )
    console.log('check nerf', getNerf)
    for (efNerf of getNerf) {
      val = nerf({
        val,
        efNerf,
        efDamage: effect,
        state,
        caster,
        target
      })
      console.log('nerf val', val)
    }
    //Boost
    let getBoost = char.status.onReceive.filter(
      x => x.type === 'boost' && x.active
    )
    for (efBoost of getBoost) {
      val = boost({
        val,
        efBoost,
        efDamage: effect,
        state,
        caster,
        target
      })
    }

    //Assign modified Val
    effect.val = val
  }

  //Return
  return effect
}

module.exports = effectInstant
