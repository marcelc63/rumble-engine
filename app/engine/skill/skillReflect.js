const _ = require('lodash')
let getChar = require('../helper/getChar.js')
let scope = require('../parsers/scope.js')

async function skillReflect(pkg) {
  //Define
  let { targeting, ally, enemy, recipient, effect, caster, state } = pkg

  //Assign
  let allChars = state[enemy].chars //Now only reflect enemy
  let onState = []
  allChars.forEach(char => {
    char.status.onState.forEach(effect => onState.push(effect))
  })
  //Can do skill thing inside here
  let onReflect = onState.filter(x => x.type === 'reflect' && x.active == true)
  console.log('Reflect triggerr', onReflect)

  //Logic
  //Counter on Attack
  if (onReflect.length > 0) {
    let reflect = onReflect[0] //Currently only recognize one reflect
    let checkScope = scope({ origin: reflect, compare: effect })
    if (!checkScope) {
      return targeting
    }
    if (
      reflect.recipient.charId === recipient.charId ||
      recipient.charId === caster.charId
    ) {
      // reflect.active = true //Effect triggered
      reflect.used = true
      console.log(reflect)
      return 'caster'
    }
  }

  //Return
  return targeting
}

module.exports = skillReflect
