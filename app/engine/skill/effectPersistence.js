let _ = require('lodash')
let getSkill = require('../helper/getSkill.js')
let getChar = require('../helper/getChar.js')
let isStun = require('../parsers/isStun')

async function effectPersistence(pkg) {
  let state = _.cloneDeep(pkg.state)
  let { caster, effect } = pkg
  let turn = state.turn % 2 === 0 ? 'even' : 'odd'

  let check = false
  let persistence = effect.persistence
  //Return
  if (persistence === 'action') {
    let stun = isStun({
      char: caster,
      compare: effect
    })
    if (stun) {
      check = true
    }
  }

  return check
}

module.exports = effectPersistence
