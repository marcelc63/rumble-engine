const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')
let nerf = require('../effects/nerf.js')

let effectInstant = require('./effectInstant.js')
let effectDuration = require('./effectDuration.js')

async function createEffect(pkg) {
  //Define
  let {
    status,
    char,
    effect,
    caster,
    target,
    turnid,
    picture,
    parent,
    name,
    state,
    recipient
  } = pkg
  let thisTurn = caster.team
  let nextTurn = caster.team === 'odd' ? 'even' : 'odd'

  //For Evaluate
  let participant = {
    //Resolving target in evaluate
    caster,
    target: recipient
  }

  //Set Options
  let valType = effect.type === 'transform' ? 'string' : 'int'

  //Permanent val modifier for instant to be added here probably
  let val =
    effect.persistence === 'instant'
      ? evaluate(
          {
            char,
            evaluatee: effect.val,
            participant,
            state
          },
          valType
        )
      : effect.val

  console.log('check val', val)

  let current = evaluate(
    {
      char,
      evaluatee: effect.val,
      participant,
      state
    },
    valType
  )

  let duration = evaluate(
    {
      char,
      evaluatee: effect.duration,
      participant,
      state
    },
    'int'
  )
  console.log('DURATION', duration)
  let active = false
  let during = effect.during === 'this turn' ? thisTurn : nextTurn

  console.log('ASSIGN DURING', effect.during, thisTurn, nextTurn)

  //Following Turn Clause
  let isFollowingTurn = false
  if (effect.during === 'following turn') {
    during = thisTurn
    duration = duration !== -1 ? duration + 1 : duration
    active = false
    isFollowingTurn = true //So it pass the EffectAssign Active Check
  }

  //Watch
  console.log('effect Before', char.name, duration, effect.isWatch)
  let watchStore = {}
  if (effect.isWatch) {
    //Define
    let type = effect.watch.type
    //Temporaries
    let tempWatchDuration = parseInt(effect.watchStore.duration)
    let tempDuration = duration
    //Modification
    console.log('inside watch', type)
    if (type === 'activation') {
      duration = tempWatchDuration
      during = effect.watchStore.during === 'this turn' ? thisTurn : nextTurn
      active = false
    }
    //Store
    watchStore = {
      duration: tempDuration,
      during: effect.during === 'this turn' ? thisTurn : nextTurn
    }
  }

  console.log('effect namee', char.name, duration)

  //Nerf if DD
  if (effect.type === 'dd') {
    let getNerf = char.status.onAttack.filter(
      x => x.type === 'nerf' && x.active
    )
    for (efNerf of getNerf) {
      current = nerf({ val: current, efNerf, efDamage: effect, state })
      console.log('nerf val', current)
    }
  }

  console.log('ASSIGN DURING 2', effect.during, thisTurn, nextTurn)

  //Return
  return {
    ...effect,
    val,
    current,
    duration,
    durationOrigin: duration,
    during,
    caster,
    target,
    recipient,
    participant,
    name,
    active,
    turnid,
    parent,
    picture,
    watchStore,
    isFollowingTurn //Following turn addition
  }
}

async function effectAssign(pkg) {
  let { status, state, turnid, char } = pkg

  //Check
  if (state.turnid !== turnid) {
    return status
  }

  //Assign Effect
  let effect = await createEffect(pkg)
  if (effect.persistence === 'instant') {
    console.log('INSTANT', effect)
    effect = await effectInstant({
      pkg,
      effect
    })
  }

  effect = await effectDuration({
    pkg,
    effect
  })

  //Check if Already Distributed
  // let distributed = status.some(
  //   x => x.type === effect.type && x.id === effect.id && x.turnid === turnid
  // )
  // if (distributed) {
  //   return status
  // }

  //Status Check
  //Refresh Effect
  if (effect.isRefresh) {
    status = status.filter(x => x.id !== effect.id)
    console.log(status)
  }

  //Check Stack
  let isStack = effect.isStack[0].value
  if (isStack) {
    let index = status.findIndex(
      x => x.type === effect.type && x.id === effect.id
    )
    if (index !== -1) {
      status[index].stack = status[index].stack + 1
      // status[index].duration = status[index].duration + 1
      return status
    }
  }

  //Consolidate
  //Check Stack
  let isConsolidate = effect.isConsolidate
  if (isConsolidate) {
    let index = status.findIndex(
      x => x.type === effect.type && x.id === effect.id
    )
    if (index !== -1) {
      status[index].val = status[index].val + effect.val
      status[index].current = status[index].current + effect.val
      return status
    }
  }

  return status.concat(effect)
}

module.exports = effectAssign
