const _ = require('lodash')
let getSkill = require('../helper/getSkill.js')
let getChar = require('../helper/getChar.js')
let skillTargeting = require('./skillTargeting.js')
let skillWatch = require('./skillWatch.js')
let skillCooldown = require('./skillCooldown')
let skillUsage = require('./skillUsage')
let persistenceCheck = require('./persistenceCheck')
let persistenceControlCheck = require('./persistenceControlCheck')
let evaluateCost = require('../parsers/cost.js')
let energyCost = require('../energy/energyCost')

async function skillQueue(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy, queue } = pkg

  //Logic
  // let allQueue = state[enemy].using.concat(queue)
  let allQueue = queue.concat(state[enemy].using)
  console.log('ALL QUEUE', allQueue)

  // for (let { item } of allQueue.map((x, i) => {
  //   return { item: x }
  // })) {
  for (let i = 0; i < allQueue.length; i++) {
    let item = allQueue[i]
    console.log('QUEUE ' + item.turnid)
    // console.log('STATE BEGINNING CYCLE', state)
    //Pre Sequence
    //Get Skill and Char
    let skill = getSkill({
      team: item.caster.team,
      charId: item.caster.charId,
      skillId: item.skillId,
      state
    })
    let char = getChar({
      team: item.caster.team,
      charId: item.caster.charId,
      state
    })

    if (item.turnid === state.turnid) {
      //Pay Energy Cost
      let cost = await evaluateCost({
        char,
        skill,
        state
      })
      state = await energyCost({
        state,
        ally,
        cost
      })

      //Set Usage
      state = await skillUsage({
        state,
        item
      })
    }

    //Persistence
    //Caster State Check -> move this inside
    let persistence = await persistenceControlCheck({ state, item })
    console.log('PERSISTENCE BEFORE', persistence, item)
    state = persistence.state
    if (persistence.check) {
      console.log('PERSISTENCE DURING', persistence)
      continue
    }
    console.log('PERSISTENCE AFTER', persistence)
    // console.log('STATE BEFORE TARGETING', state)
    //Target First Round
    state = await skillTargeting({
      state,
      char,
      ally: item.caster.team,
      enemy: item.caster.team !== 'odd' ? 'odd' : 'even',
      skill,
      item
    })

    //Watch Effect - trigger for all skills
    let lastQueue = allQueue.length === i + 1 ? true : false
    for (let i = 0; i < allQueue.length; i++) {
      let item = allQueue[i]
      state = await skillWatch({ state, ally, enemy, item, lastQueue })
    }
    // console.log('STATE AFTER WATCH', state)

    //Sequence
    //Persistence Target State Check
    state = await persistenceCheck({ state, ally, enemy, item })

    // //Post Sequence
    if (item.turnid === state.turnid) {
      //Set Cooldown
      state = await skillCooldown({
        state,
        item
      })
    }
    // console.log('STATE END CYCLE', state)
  }
  //Assign modified Queue to enemy -> Function is to recognize the changes persistenceCasterCheck did to Queue
  state[enemy].using = allQueue.filter(x => x.caster.team === enemy)

  //Return
  return state
}

module.exports = skillQueue
