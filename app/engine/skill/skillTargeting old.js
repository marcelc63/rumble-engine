const _ = require('lodash')
let getChar = require('../helper/getChar.js')
let skillSort = require('./skillSort.js')
let evaluate = require('../parsers/evaluate.js')
let isInvul = require('../parsers/isInvul.js')
let skillCounter = require('./skillCounter')

async function applyTarget(pkg) {
  let state = pkg.state
  let chars = pkg.chars
  let skill = pkg.skill
  let item = pkg.item
  let ally = pkg.ally
  let enemy = pkg.enemy
  let { effects, picture, name, persistence } = skill
  let parent = skill.id

  for (effectToClone of effects) {
    for (let { char, charId, team } of chars.map((x, i) => {
      return {
        char: x,
        charId: x.id,
        team: x.team
      }
    })) {
      let effect = _.cloneDeep(effectToClone)
      //Check Ignore Invul
      //Persistence Target Check
      let persistenceTargetCheck = true
      if (skill.persistence === 'instant' && item.turnid !== state.turnid) {
        persistenceTargetCheck = false
      }

      let ignoreInvul = evaluate({
        char,
        evaluatee: skill.isIgnoreInvul,
        participant: {
          caster: item.caster,
          target: item.target
        },
        state
      })

      if (!ignoreInvul) {
        //Exclude Invulnerable Characters
        if (team === enemy) {
          let invul = isInvul({
            char,
            compare: effect
          })
          if (invul && persistenceTargetCheck) {
            continue
          }
        }
      }

      //Assign Target
      let recipient = {
        charId: charId,
        team: team
      }

      //Sort
      state = await skillSort({
        ...item,
        recipient,
        picture,
        persistence,
        name,
        effect,
        parent,
        state,
        ally,
        enemy
      })
    }
  }

  return state
}

async function skillTargeting(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { char, skill, enemy, ally, item } = pkg
  let { caster, target } = item
  let targeting = evaluate({
    char,
    evaluatee: skill.target,
    state
  })
  console.log(targeting)

  //Targeting
  let chars = []
  if (targeting === 'self and enemy') {
    chars.push(
      getChar({
        team: target.team,
        charId: target.charId,
        state
      })
    )
    if (caster.charId !== target.charId) {
      chars.push(
        getChar({
          team: caster.team,
          charId: caster.charId,
          state
        })
      )
    }
  } else if (targeting === 'enemy') {
    chars = [
      getChar({
        team: target.team,
        charId: target.charId,
        state
      }),
      getChar({
        team: caster.team,
        charId: caster.charId,
        state
      })
    ]
  } else if (targeting === 'ally') {
    chars.push(
      getChar({
        team: target.team,
        charId: target.charId,
        state
      })
    )
    if (caster.charId !== target.charId) {
      chars.push(
        getChar({
          team: caster.team,
          charId: caster.charId,
          state
        })
      )
    }
  } else if (targeting === 'other ally') {
    chars = [
      getChar({
        team: target.team,
        charId: target.charId,
        state
      }),
      getChar({
        team: caster.team,
        charId: caster.charId,
        state
      })
    ]
  } else if (targeting === 'all') {
    if (target.team === enemy) {
      chars = state[enemy].chars.concat(state[ally].chars)
    }
    if (target.team === ally) {
      chars = state[ally].chars
    }
  } else if (targeting === 'all enemies') {
    let casterChar = getChar({
      team: caster.team,
      charId: caster.charId,
      state
    })
    chars = state[enemy].chars.concat(casterChar)
    console.log(chars)
  } else if (targeting === 'random enemy') {
    let enemies = state[enemy].chars.filter(char => {
      //Exclude Invulnerable Characters
      let invul = isInvul({
        char,
        compare: skill
      })
      if (invul) {
        return false
      }
      return true
    })
    chars.push(_.sample(enemies))
    if (caster.charId !== target.charId) {
      chars.push(
        getChar({
          team: caster.team,
          charId: caster.charId,
          state
        })
      )
    }
  } else if (targeting === 'all allies') {
    chars = state[ally].chars
  } else if (targeting === 'other allies') {
    chars = state[ally].chars.filter(x => x.id !== caster.charId)
    if (caster.charId !== target.charId) {
      chars.push(
        getChar({
          team: caster.team,
          charId: caster.charId,
          state
        })
      )
    }
  } else if (targeting === 'random ally') {
    let allies = state[ally].chars.filter(x => x.id !== caster.charId)
    chars.push(_.sample(allies))
    if (caster.charId !== target.charId) {
      chars.push(
        getChar({
          team: caster.team,
          charId: caster.charId,
          state
        })
      )
    }
  } else if (targeting === 'enemy and all allies') {
    if (caster.charId !== target.charId) {
      chars = chars.concat(state[ally].chars)
      if (caster.charId !== target.charId) {
        chars.push(
          getChar({
            team: caster.team,
            charId: caster.charId,
            state
          })
        )
      }
    }
    if (caster.charId === target.charId) {
      chars = state[ally].chars
    }
  } else {
    chars.push(
      getChar({
        team: target.team,
        charId: target.charId,
        state
      })
    )
  }

  //Counter -> Later
  console.log(chars, char)
  let counterResult = await skillCounter({
    state,
    chars,
    char,
    skill,
    ally,
    enemy,
    item
  })
  if (counterResult.counter && item.turnid === state.turnid) {
    return counterResult.state
  }

  //Target
  state = await applyTarget({
    state,
    chars,
    skill,
    targeting,
    item,
    ally,
    enemy
  })

  return state
}

module.exports = skillTargeting
