let _ = require('lodash')
let getSkill = require('../helper/getSkill.js')
let getChar = require('../helper/getChar.js')
let isStun = require('../parsers/isStun')

async function persistenceControlCheck(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { item } = pkg
  let { caster, skillId } = item
  let turn = state.turn % 2 === 0 ? 'even' : 'odd'
  let char = getChar({ team: caster.team, charId: caster.charId, state })

  let check = false

  let skill = getSkill({
    team: caster.team,
    charId: caster.charId,
    skillId,
    state
  })
  let persistence = skill.persistence

  //Return
  if (persistence === 'control') {
    let stun = isStun({ char, compare: skill })
    console.log('INSIDE CHECK before', persistence, item, stun)
    if (stun) {
      item.remove = true

      console.log('INSIDE CHECK after', persistence, item, stun)
      check = true
    }
  }
  return {
    state,
    check
  }
}

module.exports = persistenceControlCheck
