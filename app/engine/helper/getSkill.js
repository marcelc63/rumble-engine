module.exports = function getSkill(pkg) {
  //Define
  let { team, charId, skillId, state } = pkg;
  //Return
  let charIndex = state[team].chars.findIndex(x => x.id === charId);
  let skillIndex = state[team].chars[charIndex].skills.findIndex(
    x => x.id === skillId
  );
  return state[team].chars[charIndex].skills[skillIndex];
};
