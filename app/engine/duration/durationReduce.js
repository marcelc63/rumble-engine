const _ = require('lodash')
let evaluate = require('../parsers/evaluate.js')

async function durationReduce(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy, turnid, parent, caster } = pkg
  let chars = state[ally].chars.concat(state[enemy].chars)
  let turn = state.turn % 2 === 0 ? 'even' : 'odd'
  //Check
  //Logic
  for (char of chars) {
    let status = _.concat(
      char.status.onSkill,
      char.status.onReceive,
      char.status.onAttack,
      char.status.onState
    ).filter(
      x =>
        x.turnid === turnid &&
        x.parent === parent &&
        x.caster.charId === caster.charId &&
        x.caster.team === caster.team &&
        x.during === turn
    )
    for (effect of status) {
      if (effect.duration > 0) {
        effect.duration = effect.duration - 1
      }
      if (effect.type !== 'dd') {
        let valType = effect.type === 'transform' ? 'string' : 'int'
        let val = evaluate(
          {
            char,
            evaluatee: effect.val,
            state
          },
          valType
        )
        effect.current = val
      }
    }
  }
  //Return
  return state
}

module.exports = durationReduce
