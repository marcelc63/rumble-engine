const _ = require('lodash')
let energy = require('./energy/energyGenerate')
let parser = require('./parsers/parser.js')
var Char = require('../models/Char.js')

function initiate(pkg, callback) {
  // let char = getChar();
  let { players, chars, testing } = pkg

  console.log(pkg)

  Char.find({}, (err, docs) => {
    function getChar(id) {
      return JSON.parse(docs.filter(x => x._id == id)[0].data)
    }

    //Inside
    function assignChars(pkg) {
      let { turn } = pkg
      let chars = turn === 'odd' ? pkg.chars[0] : pkg.chars[1]

      //Clone Charges - Can be written more beautifully
      let allChars = pkg.chars[0].concat(pkg.chars[1])
      let charges = []
      allChars.forEach((x, i) => {
        let turn = i < 3 ? 'odd' : 'even'
        let char = _.cloneDeep(getChar(x.id))
        charges = charges.concat(
          char.status.onCharge.map(x => {
            return {
              ...x,
              caster: {
                id: char.id + '-' + turn,
                team: turn
              }
            }
          })
        )
      })

      return chars.map((x, i) => {
        let char = _.cloneDeep(getChar(x.id))
        return {
          ...char,
          hp: 100,
          team: turn,
          skills: char.skills.map(skill => {
            return {
              ...skill,
              caster: {
                id: char.id + '-' + turn,
                team: turn
              },
              effects: skill.effects.map(effect => {
                return {
                  ...effect,
                  id: effect.id
                }
              }),
              id: skill.id
            }
          }),
          status: {
            ...char.status,
            onCharge: charges.map(x => _.cloneDeep(x))
          },
          id: char.id + '-' + turn
        }
      })
    }

    let odd = {
      chars: assignChars({ chars, turn: 'odd' }),
      energy: energy(testing ? 20 : 1),
      name: players[0],
      using: []
    }
    let even = {
      chars: assignChars({ chars, turn: 'even' }),
      energy: energy(testing ? 20 : 3),
      name: players[1],
      using: []
    }

    let stateBasic = {
      odd: odd,
      even: even,
      turn: 1,
      turnid: 'turn1',
      timestamp: new Date().getTime() / 1000
    }

    let stateMeta = {
      room: pkg.room,
      channel: pkg.channel,
      log: [],
      status: {
        phase: 'ongoing', //or finish
        winner: ''
      }
    }

    let state = {
      ...stateBasic,
      ...stateMeta
    }

    //Exit
    let payload = { state }
    callback(payload)
  })
}

module.exports = initiate
