const _ = require('lodash')

function remove(chars, skillId, charId) {
  for (char of chars) {
    char.status.onReceive = char.status.onReceive.filter(
      x =>
        !(x.isEndAtDeath && x.parent === skillId && x.caster.charId === charId)
    )
    char.status.onAttack = char.status.onAttack.filter(
      x =>
        !(x.isEndAtDeath && x.parent === skillId && x.caster.charId === charId)
    )
    char.status.onSkill = char.status.onSkill.filter(
      x =>
        !(x.isEndAtDeath && x.parent === skillId && x.caster.charId === charId)
    )
    char.status.onState = char.status.onState.filter(
      x =>
        !(x.isEndAtDeath && x.parent === skillId && x.caster.charId === charId)
    )
  }
}

async function cleanupPersistence(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy } = pkg
  let chars = state[ally].chars.concat(state[enemy].chars)
  //Logic
  for (char of chars) {
    if (!char.alive) {
      char.hp = 0
    }
    if (char.hp <= 0) {
      char.hp = 0
      char.alive = false
      char.status.onReceive = []
      char.status.onAttack = []
      char.status.onSkill = []
      char.status.onState = []
      for (skill of char.skills) {
        if (char.hp <= 0) {
          remove(chars, skill.id, char.id)
        }
      }
    }
  }
  //Return
  return state
}

module.exports = cleanupPersistence
