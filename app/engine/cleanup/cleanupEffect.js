const _ = require('lodash')

function remove(status) {
  return status.filter(x => !(x.remove === true))
}

function effectMap(x, char, ally) {
  if (x.type === 'dd' && x.current === 0) {
    return {
      ...x,
      remove: true
    }
  }
  //  Implement Continuous Watch Switch Here
  if (x.watch.option === 'continuous') {
    return { ...x, active: false, isWatch: true, used: false }
  }
  return { ...x, used: false }
}

async function cleanupEffect(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy, queue } = pkg
  let chars = state[ally].chars.concat(state[enemy].chars)
  //Logic
  for (char of chars) {
    char.status.onReceive = char.status.onReceive.map(x => {
      return effectMap(x, char, ally)
    })
    char.status.onAttack = char.status.onAttack.map(x => {
      return effectMap(x, char, ally)
    })
    char.status.onSkill = char.status.onSkill.map(x => {
      return effectMap(x, char, ally)
    })
    char.status.onState = char.status.onState.map(x => {
      return effectMap(x, char, ally)
    })
  }
  for (char of chars) {
    char.status.onSkill = remove(char.status.onSkill)
    char.status.onAttack = remove(char.status.onAttack)
    char.status.onReceive = remove(char.status.onReceive)
    char.status.onState = remove(char.status.onState)
  }
  //Return
  return state
}

module.exports = cleanupEffect
