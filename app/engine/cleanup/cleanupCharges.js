const _ = require('lodash')

async function cleanupCharges(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy } = pkg
  let turnid = state.turnid
  let chars = state[ally].chars.concat(state[enemy].chars)
  //Logic
  for (char of chars) {
    let charges = char.status.onCharge
    for (charge of charges) {
      if (charge.cleanup.type === 'consume all') {
        charge.amount = 0
      }
      if (charge.cleanup.type === 'consume some') {
        let deduction = charge.amount - parseInt(charge.cleanup.amount)
        charge.amount = Math.max(deduction, 0)
      }
      //Return back to normal
      charge.cleanup.type = 'dormant'
      charge.cleanup.amount = 0
    }
  }
  //Return
  return state
}

module.exports = cleanupCharges
