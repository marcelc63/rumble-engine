const _ = require('lodash')
let evaluate = require('./evaluate.js')

//Parsers
let isAllowed = require('./isAllowed.js')
let isCooldown = require('./isCooldown.js')
let isMarking = require('./isMarking.js')
let isStun = require('./isStun')
let cost = require('./cost')
let cooldown = require('./cooldown')

async function parser(pkg) {
  //Work to parsing stuns and default skill disable
  //Define
  let state = _.cloneDeep(pkg.state)
  let { ally, enemy } = pkg
  let chars = state[ally].chars.concat(state[enemy].chars)

  //Skill Parsing
  for (let char of chars) {
    let skills = char.skills
    for (let skill of skills) {
      let skillState = {
        isStun: false
      }
      //Information
      skill.target = evaluate({
        state,
        char,
        evaluatee: skill.target
      })
      //Parse Costs
      skill.cost = await cost({
        state,
        char,
        skill
      })
      //Parse Cooldown
      skill.cooldown = cooldown({ state, char, skill })
      skill.isAllowed = isAllowed({ state, char, skill })
      skill.isCooldown = isCooldown({ state, char, skill })
      skill.isMarking = isMarking({ skill })
      skill.isIgnoreInvul = evaluate({
        char,
        evaluatee: skill.isIgnoreInvul,
        state
      })
      //Check Stun
      skillState.isStun = skill.effects.some(x => isStun({ char, compare: x }))
      //Assign Active
      if (
        skill.isAllowed === false ||
        skill.isCooldown === true ||
        skillState.isStun === true
      ) {
        skill.active = false
      } else {
        skill.active = true
      }
    }

    //Parse Status
    for (let effect of char.status.onReceive) {
      effect.val = evaluate({
        state,
        char,
        evaluatee: effect.val,
        participant: effect.participant
      })
    }
    for (let effect of char.status.onAttack) {
      effect.val = evaluate({
        state,
        char,
        evaluatee: effect.val,
        participant: effect.participant
      })
    }
    for (let effect of char.status.onSkill) {
      effect.val = evaluate({
        state,
        char,
        evaluatee: effect.val,
        participant: effect.participant
      })
    }
    for (let effect of char.status.onState) {
      effect.val = evaluate({
        state,
        char,
        evaluatee: effect.val,
        participant: effect.participant
      })
    }
  }

  //Return
  return state
}

module.exports = parser
