const _ = require('lodash')
let evaluate = require('./evaluate.js')
let isDisable = require('./isDisable.js')
let scope = require('../parsers/scope.js')

function isInvul(pkg) {
  //Define
  let { char, compare } = pkg
  //Logic
  let invul = char.status.onState.filter(x => x.type === 'invul' && x.active)
  if (invul.length > 0) {
    for (item of invul) {
      //Check Disable
      let disable = isDisable({
        char,
        compare: item
      })
      if (disable) {
        return false //Escape if ignore is true
      }
      let checkScope = scope({ origin: item, compare })
      if (!checkScope) {
        return false
      } else {
        return true
      }
    }
  }
  //Return
  return false
}

module.exports = isInvul
