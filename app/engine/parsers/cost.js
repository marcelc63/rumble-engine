const _ = require('lodash')
let evaluate = require('./evaluate.js')
let scope = require('../parsers/scope.js')

async function cost(pkg) {
  //Define
  let { char, skill, state } = pkg
  let { g, r, b, w, rd } = skill.cost
  //Logic
  let efCost = {
    g: 0,
    r: 0,
    b: 0,
    w: 0,
    rd: 0
  }
  let effects = char.status.onState.filter(x => x.type === 'cost' && x.active)
  if (effects.length > 0) {
    effects.forEach(effect => {
      let checkScope = scope({ origin: effect, compare: skill })
      if (!checkScope) {
        return
      }
      let val = evaluate(
        {
          char,
          evaluatee: effect.val,
          state
        },
        'int'
      )
      if (effect.orientation.type === 'cost') {
        if (effect.orientation.detail === 'decrease') {
          val = val * -1
        }
        if (effect.orientation.options === 'g') {
          efCost.g = val
        } else if (effect.orientation.options === 'r') {
          efCost.r = val
        } else if (effect.orientation.options === 'b') {
          efCost.b = val
        } else if (effect.orientation.options === 'w') {
          efCost.w = val
        } else if (effect.orientation.options === 'rd') {
          efCost.rd = val
        }
      }
    })
  }
  //Return
  return {
    g: evaluate({ state, char, evaluatee: g }, 'int') + efCost.g,
    r: evaluate({ state, char, evaluatee: r }, 'int') + efCost.r,
    b: evaluate({ state, char, evaluatee: b }, 'int') + efCost.b,
    w: evaluate({ state, char, evaluatee: w }, 'int') + efCost.w,
    rd: evaluate({ state, char, evaluatee: rd }, 'int') + efCost.rd
  }
}

module.exports = cost
