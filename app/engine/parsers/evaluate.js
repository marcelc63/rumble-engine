const _ = require('lodash')
let getChar = require('../helper/getChar.js')

function evaluate(pkg, type, logger = undefined) {
  //Define
  let { char, evaluatee, state, participant } = pkg
  let isArray = _.isArray(evaluatee)
  let isEmpty = evaluatee.length === 0 ? true : false
  let isMany = evaluatee.length >= 1 ? true : false

  //Logger
  if (logger !== undefined) {
    console.log('Evaluate Messages:', logger)
    console.log('Evaluate Values:', evaluatee, isArray, isEmpty, isMany)
  }

  //If Participant Empty
  if (participant === undefined) {
    participant = {
      caster: {
        team: char.team,
        charId: char.id
      },
      target: {
        team: char.team,
        charId: char.id
      }
    }
  }

  //Escape Early
  if (!isArray) {
    //Just Default Value
    //Return
    if (type === 'int') {
      return parseInt(evaluatee)
    }
    return evaluatee
  }
  if (!isMany && isEmpty) {
    //If Array is Empty
    //Return
    return true
  }
  if (!isMany && !isEmpty) {
    //If Array only have one entry
    //Return
    if (type === 'int') {
      return parseInt(evaluatee[0].value)
    }
    return evaluatee[0].value
  }

  //Parsing Logic
  let temporary = false
  if (isArray && isMany) {
    for (item of evaluatee) {
      //If Default
      if (item.type === 'default') {
        temporary = item.value
      }
      //Check Other Entires
      if (item.type !== 'default') {
        let {
          evaluator,
          condition,
          subject,
          comparison,
          value,
          direction
        } = item

        //Convert Booleans
        if (value === 'true' || value === 'false') {
          value = value === 'true' ? true : false
        }

        //Direction
        let compareWith = {}
        if (direction === 'target') {
          compareWith = char
        }
        if (direction === 'caster') {
          compareWith = getChar({
            team: participant.caster.team,
            charId: participant.caster.charId,
            state
          })
        }

        //Evaluate
        if (condition === 'active') {
          let status = _.concat(
            compareWith.status.onReceive,
            compareWith.status.onAttack,
            compareWith.status.onSkill,
            compareWith.status.onState
          )
          let compare = status.some(
            s => s.parent === comparison && s.type === subject && s.active
          )
          if (evaluator === 'exist' && compare) {
            temporary = value
            break
          }
          if (evaluator === 'nonexist' && !compare) {
            temporary = value
            break
          }
        }
        if (condition === 'toggle') {
          let status = _.concat(
            compareWith.status.onReceive,
            compareWith.status.onAttack,
            compareWith.status.onSkill,
            compareWith.status.onState
          )
          let compare = status.some(
            s =>
              s.parent === comparison &&
              s.type === subject &&
              s.active &&
              s.turnid !== state.turnid
          )
          if (evaluator === 'exist' && compare) {
            temporary = value
            break
          }
          if (evaluator === 'nonexist' && !compare) {
            temporary = value
            break
          }
        }
        if (condition === 'specific') {
          let status = _.concat(
            compareWith.status.onReceive,
            compareWith.status.onAttack,
            compareWith.status.onSkill,
            compareWith.status.onState
          )
          let compare = status.some(s => s.id === subject && s.active)
          if (evaluator === 'exist' && compare) {
            temporary = value
            break
          }
          if (evaluator === 'nonexist' && !compare) {
            temporary = value
            break
          }
        }
        if (condition === 'charge') {
          let onCharge = compareWith.status.onCharge
          let compare = onCharge.some(
            s =>
              s.id === subject &&
              s.caster.id === participant.caster.charId &&
              s.type === 'charges'
          )
          if (compare) {
            let charges = onCharge.filter(
              s =>
                s.id === subject &&
                s.caster.id === participant.caster.charId &&
                s.type === 'charges'
            )[0]
            let currentCharge = charges.amount
            let check = false
            if (evaluator === '>' && currentCharge > comparison) {
              check = true
            }
            if (evaluator === '<' && currentCharge < comparison) {
              check = true
            }
            if (evaluator === '>=' && currentCharge >= comparison) {
              check = true
            }
            if (evaluator === '<=' && currentCharge <= comparison) {
              check = true
            }
            if (evaluator === '===' && currentCharge === comparison) {
              check = true
            }
            if (evaluator === '!==' && currentCharge !== comparison) {
              check = true
            }
            if (!check) {
              continue
            }
            temporary = value
            break
          }
        }
        if (condition === 'multiply') {
          let onCharge = compareWith.status.onCharge
          let compare = onCharge.some(
            s =>
              s.id === subject &&
              s.caster.id === participant.caster.charId &&
              s.type === 'charges' &&
              s.amount > 0
          )
          if (compare) {
            let charges = onCharge.filter(
              s =>
                s.id === subject &&
                s.caster.id === participant.caster.charId &&
                s.type === 'charges' &&
                s.amount > 0
            )[0]
            let original = parseInt(evaluatee[0].value)
            let multiplier = parseInt(charges.amount)
            let addition = parseInt(value) * multiplier
            let result = original + addition
            //Message for cleanup
            charges.cleanup = {
              type: evaluator,
              amount: comparison
            }
            temporary = result
            break
          }
        }
        if (condition === 'hp') {
          let currentHP = compareWith.hp
          let check = false
          if (evaluator === '>' && currentHP > comparison) {
            check = true
          }
          if (evaluator === '<' && currentHP < comparison) {
            check = true
          }
          if (evaluator === '>=' && currentHP >= comparison) {
            check = true
          }
          if (evaluator === '<=' && currentHP <= comparison) {
            check = true
          }
          if (evaluator === '===' && currentHP === comparison) {
            check = true
          }
          if (evaluator === '!==' && currentHP !== comparison) {
            check = true
          }
          if (!check) {
            continue
          }
          temporary = value
          break
        }
        if (condition === 'multiply hp') {
          let currentHp = compareWith.hp
          let maxHp = compareWith.maxHp

          let original = parseInt(evaluatee[0].value)
          let divider = parseInt(comparison)
          let remaining = maxHp - currentHp
          let multiplier = Math.floor(remaining / divider)
          let addition = parseInt(value) * multiplier
          let result = original + addition
          console.log('interval hp', result)

          temporary = result
          break
        }
        if (condition === 'usage') {
          let skills = compareWith.skills
          let totalUsage = 0
          skills.forEach(skill => {
            if (subject.includes(skill.id)) {
              totalUsage = totalUsage + skill.usage
            }
          })
          let check = false
          if (evaluator === '>' && totalUsage > comparison) {
            check = true
          }
          if (evaluator === '<' && totalUsage < comparison) {
            check = true
          }
          if (evaluator === '>=' && totalUsage >= comparison) {
            check = true
          }
          if (evaluator === '<=' && totalUsage <= comparison) {
            check = true
          }
          if (evaluator === '===' && totalUsage === comparison) {
            check = true
          }
          if (evaluator === '!==' && totalUsage !== comparison) {
            check = true
          }
          if (!check) {
            continue
          }
          temporary = value
          break
        }
        if (condition === 'multiply usage') {
          let skills = compareWith.skills
          let totalUsage = 0
          skills.forEach(skill => {
            if (subject.includes(skill.id)) {
              totalUsage = totalUsage + skill.usage
            }
          })
          let original = parseInt(evaluatee[0].value)
          let multiplier = parseInt(totalUsage)
          let addition = parseInt(value) * multiplier
          let result = original + addition
          temporary = result
          break
        }
      }
    }
    //Return
    if (type === 'int') {
      return parseInt(temporary)
    }
    //Convert Boolean
    if (temporary === 'true') {
      temporary = true
    } else if (temporary === 'false') {
      temporary = false
    }
    //Exit
    return temporary
  }

  return false
}

module.exports = evaluate
