const _ = require('lodash')
let evaluate = require('./evaluate.js')
let scope = require('../parsers/scope.js')

function isIgnore(pkg) {
  //Define
  let { char, compare } = pkg
  //Logic
  let ignore = char.status.onState.filter(x => x.type === 'ignore' && x.active)
  if (ignore.length > 0) {
    for (item of ignore) {
      let checkScope = scope({ origin: item, compare })
      if (!checkScope) {
        return false
      } else {
        return true
      }
    }
  }
  //Return
  return false
}

module.exports = isIgnore
