const _ = require('lodash')
let evaluate = require('./evaluate.js')
let isIgnore = require('./isIgnore.js')
let scope = require('../parsers/scope.js')

function isDisable(pkg) {
  //Define
  let { char, compare } = pkg
  //Logic
  let disable = char.status.onState.filter(
    x => x.type === 'disable' && x.active
  )
  if (disable.length > 0) {
    for (item of disable) {
      //Check Ignore Stun
      let ignore = isIgnore({
        char,
        compare: item
      })
      if (ignore) {
        return false //Escape if ignore is true
      }
      //Scope
      let checkScope = scope({
        origin: item,
        compare
      })
      if (!checkScope) {
        return false
      } else {
        return true
      }
    }
  }
  //Return
  return false
}

module.exports = isDisable
