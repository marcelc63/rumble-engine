const _ = require('lodash')

function scope(pkg) {
  let { origin, compare } = pkg

  let scopeCheck = false
  let orientationCheck = true

  if (_.isObject(origin.scope)) {
    if (origin.scope.type === 'effects') {
      if (
        origin.scope.detail === 'is' &&
        origin.scope.options.includes(compare.type)
      ) {
        scopeCheck = true
      } else if (
        origin.scope.detail === 'is not' &&
        !origin.scope.options.includes(compare.type)
      ) {
        scopeCheck = true
      }
    } else if (origin.scope.type === 'classes') {
      if (
        origin.scope.detail === 'is' &&
        origin.scope.options.includes(compare.class)
      ) {
        scopeCheck = true
      } else if (
        origin.scope.detail === 'is not' &&
        !origin.scope.options.includes(compare.class)
      ) {
        scopeCheck = true
      }
    } else if (origin.scope.type === 'skills') {
      let check =
        origin.scope.options.includes(compare.id) ||
        origin.scope.options.includes(compare.parent)
      if (origin.scope.detail === 'is' && check) {
        scopeCheck = true
      } else if (origin.scope.detail === 'is not' && !check) {
        scopeCheck = true
      }
    } else if (origin.scope.type === 'all') {
      scopeCheck = true
    }
  }
  // if (_.isObject(origin.orientation)) {
  //   if (origin.orientation.type === 'remove') {
  //     let check =
  //       origin.orientation.options.includes(compare.id) ||
  //       origin.orientation.options.includes(compare.parent)
  //     if (origin.orientation.detail === 'skill' && check) {
  //       orientationCheck = true
  //     } else if (
  //       origin.orientation.detail === 'enemy' &&
  //       origin.participant.caster.team !== compare.participant.caster.team
  //     ) {
  //       orientationCheck = true
  //     }
  //   } else {
  //     orientationCheck = true
  //   }
  // }

  if (scopeCheck && orientationCheck) {
    return true
  }
  return false
}

module.exports = scope
