const _ = require("lodash");
let getChar = require("../helper/getChar.js");

function evaluate(pkg, type, logger = undefined) {
  //Define
  let { char, evaluatee, state, participant } = pkg;
  let temporary = evaluatee.length > 0 ? false : true;

  //Logic
  let object = _.isArray(evaluatee);
  if (object) {
    for (item of evaluatee) {
      if (item.type !== "default") {
        let { subject, comparison, value, direction } = item;
        //Still need to figure out best way to code direction
        let compareWith = {};
        if (direction === "target") {
          compareWith = char;
        }
        if (direction === "caster") {
          compareWith = getChar({
            team: participant.caster.team,
            charId: participant.caster.charId,
            state
          });
        }
        if (subject === "state") {
          let compare = compareWith.status.onState.some(
            s => s.parent === comparison && s.type === "state"
          );
          if (compare) {
            temporary = value;
            break;
          }
        }
        if (subject === "allow") {
          let compare = compareWith.status.onState.some(
            s => s.parent === comparison && s.type === "allow"
          );
          if (compare) {
            temporary = value;
            break;
          }
        }
      } else {
        temporary = item.value;
      }
    }
    //ToDebug
    if (logger !== undefined) {
      console.log(logger);
      console.log("DR?", temporary, evaluatee);
    }
    //Return
    if (type === "int") {
      return parseInt(temporary);
    }
    return temporary;
  } else {
    //Return
    if (type === "int") {
      return parseInt(evaluatee);
    }
    return evaluatee;
  }
}

module.exports = evaluate;
