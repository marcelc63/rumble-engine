const _ = require('lodash')
let evaluate = require('./evaluate.js')

function isAllowed(pkg) {
  //Define
  let { state, char, skill } = pkg
  //Logic
  //Max Usage
  let usage = parseInt(skill.usage)
  let maxUsage = parseInt(skill.maxUsage)
  if (skill.maxUsage > 0 && usage === maxUsage) {
    return false
  }
  //Return
  let participant = {
    caster: {
      charId: char.id,
      team: char.team
    },
    target: {
      charId: char.id,
      team: char.team
    }
  }
  let result = evaluate({
    state,
    char,
    evaluatee: skill.isAllowed,
    participant
  })
  return result
}

module.exports = isAllowed
