const _ = require('lodash')
let evaluate = require('./evaluate.js')
let scope = require('../parsers/scope.js')

function cooldown(pkg) {
  //Define
  let { char, skill, state } = pkg
  let cooldown = evaluate({ state, char, evaluatee: skill.cooldown }, 'int')
  //Logic
  let effects = char.status.onState.filter(
    x => x.type === 'cooldown' && x.active === true
  )
  if (effects.length > 0) {
    effects.forEach(effect => {
      let checkScope = scope({ origin: effect, compare: skill })
      if (!checkScope) {
        return
      }
      let val = evaluate(
        {
          char,
          evaluatee: effect.val,
          state
        },
        'int'
      )
      if (effect.orientation.detail === 'decrease') {
        val = val * -1
      }
      cooldown = cooldown + val
    })
  }
  //Return
  return cooldown
}

module.exports = cooldown
