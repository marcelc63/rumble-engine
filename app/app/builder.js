var Char = require('../models/Char.js')
var Arena = require('../models/Arena.js')
var uniqid = require('uniqid')
var passport = require('passport')
require('../config/passport')(passport)

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

module.exports = function(app) {
  app.post('/save', (req, res) => {
    let data = req.body.char
    // console.log("test", req.body.char);
    var json = JSON.stringify(req.body.char)
    if (data._id === '') {
      console.log(data.name)
      new Char({
        name: data.name,
        enable: data.enable,
        arena: data.arena,
        data: json,
        timestamp: Date.now()
      }).save(function(err, docs) {
        res.json({
          code: 200,
          id: docs.id
        })
      })
      return
    }
    //DB
    Char.findOne(
      {
        _id: data._id
      },
      (err, docs) => {
        if (docs !== null) {
          docs.name = data.name
          docs.enable = data.enable
          docs.arena = data.arena
          docs.data = json
          docs.save()
          res.json({
            code: 200,
            id: docs.id
          })
        } else {
          new Char({
            name: data.name,
            enable: data.enable,
            arena: data.arena,
            data: json,
            timestamp: Date.now()
          }).save(function(err, docs) {
            res.json({
              code: 200,
              id: docs.id
            })
          })
        }
      }
    )
  })

  app.get('/char/all', (req, res) => {
    Char.find({}, (err, docs) => {
      let result = docs.map(x => {
        return {
          _id: x._id,
          name: x.name,
          arena: x.arena
        }
      })
      res.json(result)
    })
  })

  app.get('/char/all/complete', (req, res) => {
    Char.find({ enable: true }, (err, docs) => {
      let result = docs.map(x => {
        return {
          _id: x._id,
          name: x.name,
          data: x.data,
          arena: x.arena
        }
      })
      res.json(result)
    })
  })

  app.get('/char/update', (req, res) => {
    Char.find({}, (err, docs) => {
      let result = docs.map(x => {
        // x.enable = true
        let data = JSON.parse(x.data)
        let skills = data.skills
        skills.forEach(s => {
          // x.isIgnoreReflect = [
          //   { type: 'default', value: false, schema: 'bool' }
          // ]
          s.effects.forEach(t => {
            if (t.isWatch) {
              if (t.watch.option === 'end at trigger') {
                console.log(x.name, s.name)
              }
            }
          })
          // console.log(s)
        })
        x.data = JSON.stringify(data)
        // x.save()
        return {
          _id: x._id,
          name: x.name
        }
      })
      res.json(result)
    })
  })

  app.get(
    '/char/get/:id',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
      console.log(req.params.id, req.user)
      Char.findOne({ _id: req.params.id }, (err, docs) => {
        res.json(docs)
      })
    }
  )

  app.post('/char/delete/:id', (req, res) => {
    let data = req.body.char
    Char.findOneAndDelete({ _id: data._id }, function(err, doc) {
      res.json({ code: 200 })
    })
  })

  app.get('/arena/get', async (req, res) => {
    let arena = await Arena.find({})
    res.json(arena)
  })

  app.post(
    '/arena/create',
    passport.authenticate('jwt', { session: false }),
    async (req, res) => {
      console.log('hi')

      let arena = new Arena({
        name: req.body.name,
        description: req.body.description,
        logo: req.body.logo,
        creator: req.user._id
      })
      await arena.save()

      res.json({ success: 'ok' })
    }
  )
}
