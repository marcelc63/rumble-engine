var mongoose = require('mongoose')
var Schema = mongoose.Schema
var ObjectId = mongoose.Schema.Types.ObjectId
var URLSlugs = require('mongoose-url-slugs')

var ChatSchema = new Schema({
  message: String,
  user: {
    type: ObjectId,
    ref: 'User'
  },
  username: String,
  room: String, //For Game, Private, Clan
  channel: String, //Lobby, Game, Private, Clan
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
})

ChatSchema.plugin(URLSlugs('name', { update: true }))

module.exports = mongoose.model('Chat', ChatSchema)
