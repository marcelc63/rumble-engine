var mongoose = require('mongoose')
var Schema = mongoose.Schema
var ObjectId = mongoose.Schema.Types.ObjectId
var URLSlugs = require('mongoose-url-slugs')

ArenaSchema = new Schema({
  name: String,
  description: String,
  logo: String,
  creator: {
    type: ObjectId,
    ref: 'User'
  },
  hidden: Boolean,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
})

ArenaSchema.plugin(URLSlugs('name', { update: true }))

module.exports = mongoose.model('Arena', ArenaSchema)
