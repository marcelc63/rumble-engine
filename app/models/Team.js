var mongoose = require('mongoose')
var Schema = mongoose.Schema
var ObjectId = mongoose.Schema.Types.ObjectId
var URLSlugs = require('mongoose-url-slugs')

var MySchema = new Schema({
  name: String,
  one: String,
  two: String,
  three: String,
  owner: {
    type: ObjectId,
    ref: 'User'
  },
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
})

MySchema.plugin(URLSlugs('name', { update: true }))

module.exports = mongoose.model('Team', MySchema)
