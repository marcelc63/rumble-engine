let engine = require('../engine/index.js')()
let matches = require('../store/matches.js')()
let queue = require('../store/queue.js')()
let activities = require('../store/activities.js')()

var User = require('../models/User')

module.exports = function(io, socket) {
  let user = socket.request.user
  //Sockets
  socket.on('initiate', async packet => {
    //Register Match
    let match = {
      room: packet.room
    }
    socket.join(packet.room)

    //Get Match and Prepare Payload
    let res = matches.getMatch(match)
    if (res === false) {
      return
    }
    //Parse State
    let state = await engine.parser({
      state: res.state[res.state.length - 1],
      ally: 'odd',
      enemy: 'even'
    })

    //Prepare Meta
    let ally = state.odd.name === user.username ? 'odd' : 'even'
    let enemy = state.even.name !== user.username ? 'even' : 'odd'

    //Get Profile
    let profile = {
      odd: res.testing
        ? {
            displayname: 'Tester 1',
            username: 'Tester 1',
            avatar: 'https://i.imgur.com/JUq1q2f.jpg'
          }
        : await User.findOne({
            username: state.odd.name
          }).exec(),
      even: res.testing
        ? {
            displayname: 'Tester 2',
            username: 'Tester 2',
            avatar: 'https://i.imgur.com/JUq1q2f.jpg'
          }
        : await User.findOne({
            username: state.even.name
          }).exec()
    }

    let meta = {
      mode: 'playing', //playing, spectate, replay
      channel: res.channel, //private, ladder,
      ally: ally,
      enemy: enemy,
      status: state.status,
      profile: {
        odd: {
          displayname: profile.odd.displayname,
          username: profile.odd.username,
          avatar: profile.odd.avatar
        },
        even: {
          displayname: profile.even.displayname,
          username: profile.even.username,
          avatar: profile.even.avatar
        }
      }
    }

    //Spectator
    if (state.odd.name !== user.username && state.even.name !== user.username) {
      meta = {
        ...meta,
        mode: 'spectate',
        ally: 'odd',
        enemy: 'even'
      }
    }
    //Testing
    if (res.testing) {
      meta.mode = 'testing'
    }

    console.log(meta)
    //Prepare Payload
    let payload = {
      meta: meta,
      state: state
    }
    //Emit Payload
    //Add Spectators
    activities.add({
      type: 'spectators',
      user,
      socket: socket.id,
      room: packet.room
    })
    io.to(packet.room).emit(
      'spectators',
      activities.get({
        type: 'spectators',
        room: packet.room
      })
    )

    socket.emit('initiate', payload)
  })

  socket.on('testing', packet => {
    console.log('test')
    queue.testing({}, x => {
      io.to(socket.id).emit('found', {
        room: x.room,
        testing: true
      })

      //Register Match
      let match = {
        players: ['Tester 1', 'Tester 2'], //Later take from session
        room: x.room,
        chars: [packet.chars, packet.chars],
        channel: 'test',
        testing: true
      }
      //Initiate Match
      matches.initiateMatch(match)
    })
  })

  socket.on('battle', packet => {
    //Match Data
    let pkgMatch = {
      room: packet.room
    }
    let match = matches.getMatch(pkgMatch)
    if (match === false) {
      return
    }
    //Engine Data
    let pkgEngine = {
      state: match.state[match.state.length - 1],
      action: packet.action,
      redeem: packet.redeem,
      exchange: packet.exchange
    }

    //Sanity Check
    let turn = pkgEngine.state.turn % 2 === 0 ? 'even' : 'odd'
    if (pkgEngine.state[turn].name !== user.username && !match.testing) {
      return
    }

    //Process  Data
    engine.battle(pkgEngine, res => {
      let pkgUpdate = {
        room: packet.room,
        state: res.state,
        status: res.state.status
      }
      matches.updateMatch('state', pkgUpdate)
      let meta = {
        status: res.state.status
      }
      let pkgEmit = {
        state: res.view,
        meta
      }
      io.to(packet.room).emit('result', pkgEmit)

      //Remove Activity
      if (meta.status.phase === 'finish') {
        activities.remove({
          type: 'matches',
          room: packet.room
        })
        io.emit('setup', activities.get())
        matches.removeMatch(packet.room)
      }
    })
  })

  socket.on('search', packet => {
    let pkgSearch = {
      player: user.username,
      profile: user,
      chars: packet.chars,
      socket: socket.id,
      channel: packet.channel
    }

    if (packet.channel === 'private') {
      pkgSearch.opponent = packet.opponent.username
      console.log(pkgSearch)
    }

    queue.search(pkgSearch, x => {
      io.to(x.socket).emit('found', {
        player: x.player,
        room: x.room,
        testing: false
      })
      io.to(socket.id).emit('found', {
        player: user.username,
        room: x.room,
        testing: false
      })

      //Register Match
      let match = {
        players: [x.player, user.username], //Later take from session
        room: x.room,
        chars: [x.chars, packet.chars],
        channel: x.channel,
        testing: false
      }

      //Initiate Match
      matches.initiateMatch(match)
      activities.add({
        type: 'matches',
        match,
        profile: {
          odd: x.profile,
          even: user
        }
      })
      io.emit('setup', activities.get())
    })
  })

  socket.on('cancel', packet => {
    let pkgCancel = {
      player: user.username
    }
    queue.cancel(pkgCancel)
  })

  socket.on('surrender', async packet => {
    console.log('Surrender')
    //Match Data
    let pkgMatch = {
      room: packet.room
    }
    let match = matches.getMatch(pkgMatch)
    if (match === false) {
      return
    }
    //Surrender
    state = match.state[match.state.length - 1]

    let ally = state.turn % 2 === 0 ? 'even' : 'odd'
    let enemy = state.turn % 2 !== 0 ? 'even' : 'odd'

    state.status = {
      phase: 'finish', //or finish
      winner: state.odd.name === user.username ? 'even' : 'odd'
    }

    let pkgUpdate = {
      room: packet.room,
      state: state,
      status: state.status
    }
    matches.updateMatch('state', pkgUpdate)

    let view = await engine.parser({
      state,
      ally,
      enemy
    })

    let meta = {
      status: state.status
    }

    let pkgEmit = {
      state: view,
      meta
    }
    console.log('pkgEngine', pkgEmit)
    io.to(packet.room).emit('result', pkgEmit)

    //Remove Activity
    activities.remove({
      type: 'matches',
      room: packet.room
    })
    io.emit('setup', activities.get())
    matches.removeMatch(packet.room)
  })

  socket.on('leave', packet => {
    activities.remove({ type: 'spectators', user })
    io.to(packet.room).emit(
      'spectators',
      activities.get({
        type: 'spectators',
        room: packet.room
      })
    )
    socket.leave(packet.room)
  })
}
