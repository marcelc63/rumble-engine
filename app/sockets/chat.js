var Chat = require('../models/Chat')

module.exports = function(io, socket) {
  let user = socket.request.user

  socket.on('chat', packet => {
    if (packet.channel === 'lobby') {
      io.emit('msg', {
        displayname: user.displayname,
        username: user.username,
        room: packet.room,
        channel: packet.channel,
        message: packet.message,
        datestamp: Date.now()
      })
    }
    if (packet.channel === 'game') {
      console.log('game chat', packet)
      io.to(packet.room).emit('msg', {
        displayname: user.displayname,
        username: user.username,
        room: packet.room,
        channel: packet.channel,
        message: packet.message,
        datestamp: Date.now()
      })
    }

    // let payload = {
    //   user: user._id,
    //   username: user.username,
    //   room: packet.room,
    //   channel: packet.channel,
    //   message: packet.message
    // }

    // Chat.create(payload, function(err, post) {
    //   if (err) return console.log(err)
    //   res.json(post)
    // })
  })

  socket.on('load', packet => {})
}
