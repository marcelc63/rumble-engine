let activities = require('../store/activities.js')()

module.exports = function(io, socket) {
  let user = socket.request.user

  socket.on('lobby', packet => {
    activities.add({ type: 'online', user, socket: socket.id })
    io.emit('setup', activities.get())
  })

  socket.on('logout', packet => {
    activities.remove({ type: 'online', user })
    io.emit('setup', activities.get())
  })

  socket.on('disconnect', packet => {
    activities.remove({ type: 'online', user })
    activities.remove({ type: 'spectators', user })
    io.emit('setup', activities.get())
  })
}
