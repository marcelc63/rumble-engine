let engine = require("../engine/index.js")();
let matches = require("../store/matches.js")();

module.exports = function(io) {
  //Timer
  setInterval(() => {
    let now = new Date().getTime() / 1000;
    let allMatches = matches
      .getAll()
      .filter(match => match.status === "ongoing" && !match.testing);
    allMatches.forEach(match => {
      let state = match.state[match.state.length - 1];
      let turn = match.state.length % 2 === 0 ? "even" : "odd";
      let timestamp = state.timestamp;
      let difference = Math.floor(now - timestamp);
      if (difference === 60) {
        let pkgEngine = {
          state: state,
          action: state[turn].using,
          redeem: {
            g: 0,
            r: 0,
            b: 0,
            w: 0
          },
          exchange: {
            receve: "",
            offer: {
              g: 0,
              r: 0,
              b: 0,
              w: 0
            }
          }
        };
        console.log("pkgEngine", pkgEngine);
        engine.battle(pkgEngine, res => {
          let pkgUpdate = {
            room: match.room,
            state: res.state,
            status: res.state.status
          };
          matches.updateMatch("state", pkgUpdate);
          let meta = {
            status: res.state.status
          };
          let pkgEmit = {
            state: res.view,
            meta
          };
          console.log("pkgEngine", pkgEmit);
          io.to(match.room).emit("result", pkgEmit);
        });
      }
    });
  }, 1000);
};
