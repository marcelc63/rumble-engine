const uniqid = require('uniqid')
let engine = require('../engine/index.js')()

//Functions
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max))
}

function queue() {
  let queue = []

  function matchMaking(payload, callback) {
    let pool = queue.length
    let exist = queue.some(
      x => x.player === payload.player && x.channel === payload.channel
    )
    if (!exist) {
      if (pool > 0) {
        //There's Opponent
        let opponent =
          payload.channel === 'private'
            ? queue.filter(
                x =>
                  x.player === payload.opponent && x.channel === payload.channel
              )[0]
            : queue.filter(x => x.channel === payload.channel)[
                getRandomInt(pool)
              ]
        queue = queue.filter(x => x.player !== opponent.player) //Remove Opponent from Queue
        callback(opponent) //Callback to Getter
      } else {
        //No Opponent
        queue.push({
          player: payload.player,
          profile: payload.profile,
          chars: payload.chars,
          socket: payload.socket,
          channel: payload.channel,
          room: uniqid.time()
        }) //Add self to Queue
      }
    }
  }

  function matchMakingCancel(payload, callback) {
    queue = queue.filter(x => x.player !== payload.player) //Remove Opponent from Queue
  }

  function matchTesting(payload, callback) {
    callback({
      room: uniqid.time()
    }) //Callback to Getter
  }

  //expose
  return {
    search: matchMaking,
    cancel: matchMakingCancel,
    testing: matchTesting
  }
}

module.exports = queue
