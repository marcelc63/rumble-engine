let engine = require('../engine/index.js')()

//Define
let match = {
  players: [],
  room: '',
  channel: '',
  state: [],
  status: '',
  winner: ''
}

//Store
let matchStore = []

function matches() {
  //Operators
  const setMatch = pkg => {
    // if ((pkg.testing = true)) {
    //   pkg.players = ["Tester 1", "Tester 2"];
    // }
    engine.initiate(pkg, instance => {
      let match = {
        players: pkg.players,
        room: pkg.room,
        channel: pkg.channel,
        state: [instance.state],
        chars: pkg.chars,
        testing: pkg.testing,
        status: 'ongoing',
        winner: ''
      }
      matchStore = matchStore.concat(match)
    })
  }
  const getMatch = pkg => {
    let match = matchStore.filter(x => x.room === pkg.room)
    if (match.length === 0) {
      return false
    }
    return match[0]
  }
  const getAll = pkg => {
    return matchStore
  }
  const updateMatch = (type, pkg) => {
    let index = matchStore.findIndex(x => x.room === pkg.room)
    let match = matchStore[index]
    if (type === 'state') {
      match.state = match.state.concat(pkg.state)
      match.status = pkg.status.phase
      if (pkg.status.status === 'finish') {
        match.winner = pkg.status.winner
      }
    } else if (type === 'player') {
      match.players = match.players.concat(pkg.player)
    }
  }

  //Expose
  return {
    initiateMatch: (pkg, cb) => {
      let match = getMatch(pkg)
      if (match === false) {
        setMatch(pkg)
      }
    },
    setMatch,
    getMatch,
    getAll,
    updateMatch,
    removeMatch: room => {
      matchStore = matchStore.filter(x => x.room === room)
    }
  }
}

module.exports = matches
