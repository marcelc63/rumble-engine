const uniqid = require('uniqid')

let activities = {
  online: [],
  matches: [],
  spectators: []
}

function activity() {
  function activityAdd(payload, callback) {
    let { type } = payload
    if (type === 'online') {
      let { user, socket } = payload
      let check = activities.online.some(x => x.username === user.username)
      if (!check) {
        activities.online.push({
          displayname: user.displayname,
          username: user.username,
          socket
        })
      }
    }
    if (type === 'matches') {
      let { match, profile } = payload
      let check = activities.matches.some(x => x.room === match.room)
      if (!check) {
        activities.matches.push({
          room: match.room,
          title: `${profile.odd.displayname} vs ${profile.even.displayname}`
        })
      }
    }
    if (type === 'spectators') {
      let { user, socket, room } = payload
      let check = activities.spectators.some(x => x.username === user.username)
      if (!check) {
        activities.spectators.push({
          displayname: user.displayname,
          username: user.username,
          socket,
          room
        })
      }
    }
  }

  function activityRemove(payload, callback) {
    let { type } = payload
    if (type === 'online') {
      let { user } = payload
      activities.online = activities.online.filter(
        x => x.username !== user.username
      )
    }
    if (type === 'matches') {
      let { room } = payload
      activities.matches = activities.matches.filter(x => x.room !== room)
    }
    if (type === 'spectators') {
      let { user } = payload
      activities.spectators = activities.spectators.filter(
        x => x.username !== user.username
      )
    }
  }

  function activityGet(payload = {}, callback) {
    let { type } = payload
    if (type === 'spectators') {
      let { room } = payload
      return activities.spectators.filter(x => x.room === room)
    }
    return activities
  }

  //expose
  return {
    add: activityAdd,
    remove: activityRemove,
    get: activityGet
  }
}

module.exports = activity
