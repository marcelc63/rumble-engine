var express = require('express')
var router = express.Router()
var Char = require('../models/Char.js')
var Arena = require('../models/Arena.js')
var passport = require('passport')
require('../config/passport')(passport)

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

/* GET ALL */
router.get('/char/get/all', function(req, res) {
  Char.find({}, (err, docs) => {
    let result = docs.map(x => {
      return { _id: x._id, name: x.name, arena: x.arena }
    })
    res.json(result)
  })
})

router.get('/char/get/all/complete', function(req, res) {
  console.log('test')
  Char.find({}, (err, docs) => {
    let result = docs.map(x => {
      return { _id: x._id, name: x.name, data: x.data, arena: x.arena }
    })
    res.json(result)
  })
})

/* GET ONE */
router.get(
  '/char/get/:id',
  passport.authenticate('jwt', { session: false }),
  function(req, res) {
    var token = getToken(req.headers)
    if (token) {
      Char.findOne({ _id: req.params.id }, function(err, docs) {
        if (err) return next(err)
        res.json(docs)
      })
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

/* SAVE & UPDATE CHAR*/
async function createChar(data, json) {
  let char = await new Char({
    name: data.name,
    enable: data.enable,
    arena: data.arena,
    data: json,
    timestamp: Date.now()
  })
  await char.save()
  return { code: 200, id: char.id }
}

router.post(
  '/char/save',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let token = getToken(req.headers)
    if (token) {
      let data = req.body.char
      let json = JSON.stringify(req.body.char)
      if (data._id === '') {
        let result = await createChar(data, json)
        return res.json(result)
      } else {
        let char = await Char.findOne({ _id: data._id })
        if (char !== null) {
          char.name = data.name
          char.enable = data.enable
          char.arena = data.arena
          char.data = json
          await char.save()
          res.json({ code: 200, id: docs.id })
        } else {
          let result = await createChar(data, json)
          return res.json(result)
        }
      }
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

/* DELETE CHAR */
router.post(
  '/char/delete/:id',
  passport.authenticate('jwt', { session: false }),
  async function(req, res) {
    let token = getToken(req.headers)
    if (token) {
      let data = req.body.char
      await Char.findOneAndDelete({ _id: data._id })
      res.json({ code: 200 })
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

/* GET ARENA */
router.get('/arena/get/all', async function(req, res) {
  let arena = await Arena.find({})
  res.json(arena)
})

router.get('/arena/get/:id', async function(req, res) {
  let arena = await Arena.findOne({ _id: req.params.id })
  res.json(arena)
})

/* UPDATE */
router.post(
  '/arena/create',
  passport.authenticate('jwt', { session: false }),
  async function(req, res) {
    var token = getToken(req.headers)
    if (token) {
      let arena = new Arena({
        name: req.body.name,
        description: req.body.description,
        logo: req.body.logo,
        creator: req.user._id
      })
      await arena.save()

      res.json({ success: 'ok' })
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

router.post(
  '/arena/update/:id',
  passport.authenticate('jwt', { session: false }),
  async function(req, res) {
    var token = getToken(req.headers)
    if (token) {
      let payload = {
        name: req.body.name,
        description: req.body.description,
        logo: req.body.logo,
        creator: req.user._id
      }

      let arenaUpdated = await Arena.findOneAndUpdate(
        { _id: req.params.id },
        payload,
        {
          new: true
        }
      )
      res.json(arenaUpdated)
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

module.exports = router
