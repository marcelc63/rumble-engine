var express = require('express')
var router = express.Router()
var Model = require('../models/Team.js')
var passport = require('passport')
require('../config/passport')(passport)

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

/* GET ALL */
router.get('/', passport.authenticate('jwt', { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers)
  if (token) {
    Model.find({ owner: req.user._id }, function(err, docs) {
      if (err) return next(err)      
      res.json(docs)
    })
  } else {
    return res.status(403).send({ success: false, msg: 'Unauthorized.' })
  }
})

/* GET ONE */
router.get('/:id', function(req, res) {
  Model.findOne({ _id: req.params.id }, function(err, docs) {
    if (err) return next(err)
    console.log(docs)
    let payload = {
      name: docs.name,
      one: docs.one,
      two: docs.two,
      three: docs.three
    }
    res.json(payload)
  })
})

/* SAVE */
router.post('/', passport.authenticate('jwt', { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers)
  if (token) {
    let payload = {
      name: req.body.name,
      one: req.body.one,
      two: req.body.two,
      three: req.body.three,
      owner: req.user._id
    }
    Model.create(payload, function(err, post) {
      if (err) return console.log(err)
      res.json(post)
    })
  } else {
    return res.status(403).send({ success: false, msg: 'Unauthorized.' })
  }
})

/* UPDATE */
router.post(
  '/update/:id',
  passport.authenticate('jwt', { session: false }),
  function(req, res, next) {
    var token = getToken(req.headers)
    if (token) {
      let payload = {
        name: req.body.name,
        one: req.body.one,
        two: req.body.two,
        three: req.body.three
      }

      Model.findOneAndUpdate(
        { _id: req.params.id },
        payload,
        { new: true },
        function(err, post) {
          if (err) return next(err)
          res.json(post)
        }
      )
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

/* DELETE */
router.post(
  '/delete/:id',
  passport.authenticate('jwt', { session: false }),
  function(req, res) {
    // console.log("delete");
    var token = getToken(req.headers)
    if (token) {
      Model.findByIdAndRemove(req.params.id, function(err, post) {
        if (err) return console.log(err)
        console.log('done')
        res.json(post)
      })
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

module.exports = router
