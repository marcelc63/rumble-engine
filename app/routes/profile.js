var express = require('express')
var router = express.Router()
var Model = require('../models/User.js')
var passport = require('passport')
require('../config/passport')(passport)

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ')
    if (parted.length === 2) {
      return parted[1]
    } else {
      return null
    }
  } else {
    return null
  }
}

/* GET ALL */
router.get('/', passport.authenticate('jwt', { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers)
  if (token) {
    Model.findOne({ _id: req.user._id }, function(err, docs) {
      if (err) return next(err)
      let payload = {
        background: docs.background,
        avatar: docs.avatar,
        displayname: docs.displayname,
        username: docs.username,
        team: docs.team
      }
      res.json(payload)
    })
  } else {
    return res.status(403).send({ success: false, msg: 'Unauthorized.' })
  }
})

/* GET ONE */
router.get('/:id', function(req, res) {
  Model.findOne({ _id: req.params.id }, function(err, docs) {
    if (err) return next(err)
    console.log(docs)
    let payload = {
      background: docs.background,
      avatar: docs.avatar,
      displayname: docs.displayname,
      username: docs.username
    }
    res.json(payload)
  })
})

/* SAVE */
router.post('/', passport.authenticate('jwt', { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers)
  if (token) {
    let payload = {
      ...req.body,
      content: JSON.stringify(req.body.content),
      owner: req.user._id
    }
    // console.log(payload);
    // return res.json(403);
    Model.create(payload, function(err, post) {
      if (err) return console.log(err)
      // console.log("done");
      res.json(post)
    })
  } else {
    return res.status(403).send({ success: false, msg: 'Unauthorized.' })
  }
})

/* UPDATE */
router.post(
  '/update/team',
  passport.authenticate('jwt', { session: false }),
  function(req, res, next) {
    // console.log("update");
    var token = getToken(req.headers)
    if (token) {
      let payload = {
        team: req.body.team
      }
      Model.findOneAndUpdate({ _id: req.user._id }, payload, function(
        err,
        post
      ) {
        if (err) return next(err)
        res.json({ success: true, msg: 'Complete.' })
      })
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

router.post(
  '/update',
  passport.authenticate('jwt', { session: false }),
  function(req, res, next) {
    // console.log("update");
    var token = getToken(req.headers)
    if (token) {
      let payload = {
        background: req.body.background,
        avatar: req.body.avatar,
        displayname: req.body.displayname
      }
      Model.findOneAndUpdate(
        { _id: req.user._id },
        payload,
        { new: true },
        function(err, post) {
          if (err) return next(err)
          res.json(post)
        }
      )
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

/* DELETE */
router.post(
  '/delete/:id',
  passport.authenticate('jwt', { session: false }),
  function(req, res) {
    // console.log("delete");
    var token = getToken(req.headers)
    if (token) {
      Model.findByIdAndRemove(req.params.id, function(err, post) {
        if (err) return console.log(err)
        res.json(post)
      })
    } else {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' })
    }
  }
)

module.exports = router
