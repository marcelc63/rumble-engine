var passport = require('passport')
var settings = require('../config/settings')
require('../config/passport')(passport)
var express = require('express')
var jwt = require('jsonwebtoken')
var router = express.Router()
var User = require('../models/User')

router.post('/register', function(req, res) {
  if (!req.body.username || !req.body.password) {
    res.json({ success: false, msg: 'Please pass username and password.' })
  } else {
    var newUser = new User({
      username: req.body.username.toLowerCase(),
      displayname: req.body.displayname,
      email: req.body.email,
      password: req.body.password,
      usergroup: 'member',
      avatar: 'https://i.imgur.com/dEfJ24E.png',
      background: {
        ingame: 'https://images8.alphacoders.com/101/1018912.jpg',
        lobby: 'https://images5.alphacoders.com/103/1033113.png',
        profile: 'https://images5.alphacoders.com/103/1033113.png'
      }
    })
    // save the user
    newUser.save(function(err) {
      console.log('hi3')
      if (err) {
        console.log(err)
        return res.json({ success: false, msg: 'Username already exists.' })
      }
      console.log('hi4')
      res.json({ success: true, msg: 'Successful created new user.' })
    })
  }
})

router.post('/login', function(req, res) {
  User.findOne(
    {
      username: req.body.username.toLowerCase()
    },
    function(err, user) {
      if (err) throw err

      if (!user) {
        res.status(401).send({
          success: false,
          msg: 'Authentication failed. User not found.'
        })
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user.toJSON(), settings.secret)
            // return the information including token as JSON
            res.json({ success: true, token: token, usergroup: user.usergroup })
          } else {
            res.status(401).send({
              success: false,
              msg: 'Authentication failed. Wrong password.'
            })
          }
        })
      }
    }
  )
})

router.post('/logout', function(req, res) {
  req.logout()
  res.json({ success: true })
})

module.exports = router
