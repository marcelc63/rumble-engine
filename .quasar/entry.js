/**
 * THIS FILE IS GENERATED AUTOMATICALLY.
 * DO NOT EDIT.
 *
 * You are probably looking on adding initialization code.
 * Use "quasar new plugin <name>" and add it there.
 * One plugin per concern. Then reference the file(s) in quasar.conf.js > plugins:
 * plugins: ['file', ...] // do not add ".js" extension to it.
 **/
import './quasar'

import Vue from 'vue'
Vue.config.productionTip = true


import '../src-pwa/register-service-worker.js'



import 'quasar-extras/roboto-font'

import 'quasar-extras/material-icons'

import 'quasar-extras/ionicons'




import 'quasar-app-styl'


import 'src/css/app.styl'


import App from '../src/App.vue'

import router from '../src/router'

import store from '../src/store'


const app = {
  el: '#q-app',
  router,
store,
  ...App
}


const plugins = []

import pluginI18n from 'src/plugins/i18n'
plugins.push(pluginI18n)

import pluginAxios from 'src/plugins/axios'
plugins.push(pluginAxios)

import pluginSocket from 'src/plugins/socket'
plugins.push(pluginSocket)

import pluginChatscroll from 'src/plugins/chatscroll'
plugins.push(pluginChatscroll)

plugins.forEach(plugin => plugin({ app, router, store, Vue }))





import FastClick from 'fastclick'
// Needed only for iOS PWAs
if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream && window.navigator.standalone) {
  document.addEventListener('DOMContentLoaded', () => {
    FastClick.attach(document.body)
  }, false)
}





new Vue(app)



