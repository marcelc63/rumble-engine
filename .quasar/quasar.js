/**
 * THIS FILE IS GENERATED AUTOMATICALLY.
 * DO NOT EDIT.
 *
 * You are probably looking on adding initialization code.
 * Use "quasar new plugin <name>" and add it there.
 * One plugin per concern. Then reference the file(s) in quasar.conf.js > plugins:
 * plugins: ['file', ...] // do not add ".js" extension to it.
 **/


import Vue from 'vue'
import Quasar, {QLayout,QLayoutHeader,QLayoutFooter,QLayoutDrawer,QPageContainer,QPage,QToolbar,QToolbarTitle,QBtn,QIcon,QList,QListHeader,QItem,QItemMain,QItemSide,QModal,QTooltip,QCard,QCardTitle,QCardMain,QCardMedia,QCardSeparator,QCardActions,QInput,QCheckbox,QField,QToggle,QSelect,QTabs,QTab,QTabPane,QRouteTab,QPageSticky,Ripple,Notify,LocalStorage,SessionStorage} from 'quasar'

Vue.use(Quasar, { cfg: {},components: {QLayout,QLayoutHeader,QLayoutFooter,QLayoutDrawer,QPageContainer,QPage,QToolbar,QToolbarTitle,QBtn,QIcon,QList,QListHeader,QItem,QItemMain,QItemSide,QModal,QTooltip,QCard,QCardTitle,QCardMain,QCardMedia,QCardSeparator,QCardActions,QInput,QCheckbox,QField,QToggle,QSelect,QTabs,QTab,QTabPane,QRouteTab,QPageSticky},directives: {Ripple},plugins: {Notify,LocalStorage,SessionStorage} })
